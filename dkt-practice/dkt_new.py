from os import stat_result
import numpy as np
import torch
from torch.nn import Module, Parameter, Embedding 
from torch import nn
from torch.nn.functional import one_hot, binary_cross_entropy
from torch.nn.init import normal_
from sklearn import metrics

if torch.cuda.is_available():
    torch.set_default_tensor_type(torch.cuda.FloatTensor)

class DKT(Module):
    def __init__(self, hidden_size, num_kcs):
        super(DKT, self).__init__()
        # hyper parameters
        self.hidden_size = hidden_size
        self.num_kcs = num_kcs

        # model parameters
        self.W_hx = Parameter(torch.Tensor(self.hidden_size, self.hidden_size))
        self.W_hh = Parameter(torch.Tensor(self.hidden_size, self.hidden_size))
        self.b_h = Parameter(torch.Tensor(self.hidden_size))
        self.interaction_emb = Embedding(self.num_kcs * 2, self.hidden_size)


        self.W_yh = Parameter(torch.Tensor(self.num_kcs, self.hidden_size))
        self.b_y = Parameter(torch.Tensor(self.num_kcs))
        # self.h_t = nn.init.zeros_(torch.Tensor(self.hidden_size))

        normal_(self.W_hx)
        normal_(self.W_hh)
        # normal_(self.b_h)
        normal_(self.W_yh)
        # normal_(self.b_y)

    def forward(self, q, r, h_t = None): 
        qr = np.array(self.num_kcs * r + q)
        # hot = one_hot(torch.tensor(qr).to(torch.long), 2 * self.num_kcs)
        # hot = one_hot(torch.tensor(qr.flatten()).to(torch.long), 2 * self.num_kcs)
        if h_t == None: 
            h_t = nn.init.zeros_(torch.Tensor(self.hidden_size))

        # looping through number of batch_size students * total number of questions solved 
        y = torch.tensor([])
        for idx1, one_student in enumerate(qr): 
            yy = torch.tensor([])
            for idx2, one_question in enumerate(one_student): 
                # x_t = hot[idx1][idx2].to(torch.float)
                x_t = self.interaction_emb(torch.tensor(qr[idx1][idx2]).to(torch.long))
                h_t = torch.tanh(torch.matmul(self.W_hx, x_t) + torch.matmul(self.W_hh, h_t) + self.b_h)
                prob = torch.sigmoid(torch.matmul(self.W_yh, h_t) + self.b_y)
                yy = torch.cat((yy, prob), -1)
            y = torch.cat((y, yy), -1)
        y = torch.reshape(y, (len(q), len(q[0]), self.num_kcs))
        print("y size: ", y.size())
        return y, h_t 
    
    def train_model(self, train, test, q2idx, num_epochs, hidden_size, opt, batch_size):
        h_t = None
        aucs, loss_means = [], []
        
        q, r, t, d, m = train
        q_test, r_test, t_test, d_test, m_test = test

        for i in range(1, num_epochs + 1):
            auc, loss_mean = [], []
            bch_idx = np.random.choice(len(q), batch_size, replace = False)
            q_seq, r_seq, t_seq, d_seq, m_seq = q[bch_idx], r[bch_idx], t[bch_idx], d[bch_idx], m[bch_idx]

            self.train()
            if i > 1:
                print("h_t", h_t)
            y, state = self(q_seq, r_seq, h_t)
            # print(y)
            h_t = state
            temp = one_hot(torch.tensor(d_seq).to(torch.long), self.num_kcs)
            y = (y * temp).sum(-1)
            y_result = torch.masked_select(y, torch.tensor(m_seq))
            print("y_result: ", y_result)
            t_result = torch.masked_select(torch.tensor(t_seq), torch.tensor(m_seq))

            opt.zero_grad()
            loss = binary_cross_entropy(y_result, t_result)
            
            # for param in self.parameters():
            #     print("gradient")
            #     print(param.data)

            loss.backward()
            opt.step() 

            loss_mean.append(loss.detach().cpu().numpy())
            print(loss_mean)
            #test
            # with torch.no_grad():
            #     bch_idx = np.random.choice(len(q_test), batch_size, replace = False)
            #     q_seq, r_seq, t_seq, d_seq, m_seq = q_test[bch_idx], r_test[bch_idx], t_test[bch_idx], d_test[bch_idx], m_test[bch_idx]    
            #     self.eval() 

            #     y, state = self(q_seq, r_seq)
            #     print("y:", y)
            #     temp = one_hot(torch.tensor(d_seq).to(torch.long), self.num_kcs)
            #     y = (y * temp).sum(-1)

            #     y_result = torch.masked_select(y, torch.tensor(m_seq)).detach().cpu()
            #     print("y_result", y_result)
            #     t_result = torch.masked_select(torch.tensor(t_seq), torch.tensor(m_seq)).detach().cpu()

            #     auc = metrics.roc_auc_score(
            #         y_true = t_result.numpy(), y_score = y_result.numpy()
            #     )

            #     loss_mean = np.mean(loss_mean)
            #     print(
            #         "Epoch: {},   AUC: {},   Loss Mean: {}"
            #         .format(i, auc, loss_mean)
            #     )

            #     aucs.append(auc)
            #     loss_means.append(loss_mean)

        return loss_means 


                


       





        #     for _ in range(len(q_seq) // batch_size):
        #         bch_idx = np.random.choice(len(q_seq), batch_size, replace = False)
           
        #         for idx in bch_idx: 
        #             q_seq_oneuser = q_seq[idx]
        #             r_seq_oneuser = r_seq[idx]
                    
        #             self.train()
        #             T = len(q_seq_oneuser)
        #             # print("Length of one user: ",T)
        #             pred, actual = [], []
        #             for timestamp in range(0, T-1):
        #                 y, state = self(q_seq_oneuser, r_seq_oneuser, q2idx, timestamp, h_t)
        #                 state = state.detach()
        #                 # get the probability of timestamp + 1 
        #                 d = q2idx[q_seq_oneuser[timestamp + 1]] 

        #                 pred.append(y[d])
        #                 actual.append(r_seq_oneuser[timestamp + 1])
        #                 h_t = state
                
        #             opt.zero_grad()

        #             # print("length of pred: ",len(pred))
        #             # print("pred: ",torch.stack(pred))
        #             # print("actual: ", actual)            

        #             pred = torch.stack(pred)
        #             actual = torch.FloatTensor(actual).clone()
        #             loss = binary_cross_entropy(pred, actual).mean()
        #             # print("loss: ",loss)
        #             loss.backward()
        #             opt.step()

        #             loss_mean.append(loss.detach().cpu().numpy())

        #     with torch.no_grad():
        #         for _ in range(len(q_seq_test) // batch_size):
        #             bch_idx = np.random.choice(len(q_seq_test), batch_size, replace = False )
        #             # print("test bch_idx: ", bch_idx)
        #             for idx in bch_idx:
        #                 q_seq_oneuser = q_seq_test[idx]
        #                 r_seq_oneuser = r_seq_test[idx]
        #                 self.eval() 

        #                 T = len(q_seq_oneuser)
        #                 # print("Length of one user: ",T)
        #                 pred, actual = [], []
        #                 for timestamp in range(0, T-1):
        #                     y, state = self(q_seq_oneuser, r_seq_oneuser, q2idx, timestamp, h_t)
        #                     state = state.detach()
        #                     # get the probability of timestamp + 1 
        #                     d = q2idx[q_seq_oneuser[timestamp + 1]] 

        #                     pred.append(y[d].item())
        #                     actual.append(r_seq_oneuser[timestamp + 1])
        #                     h_t = state
        #                     # print("test actual: ", actual)
                            
        #         auc = metrics.roc_auc_score(np.array(actual), np.array(pred))
        #         loss_mean = np.mean(loss_mean)
                
        #         print(
        #             "Epoch: {},   AUC: {},   Loss Mean: {}"
        #         .format(i, auc, loss_mean))

        #         aucs.append(auc)
        #         loss_means.append(loss_mean)
                            
            
        
        # return aucs, loss_means



                

                # loss_mean.append(loss.detach().cpu().numpy())

            # for q_seq_oneuser, r_seq_oneuser in zip(q_seq,r_seq):
                
                
            #     self.train()
            #     y = self(q_seq_oneuser, r_seq_oneuser, q2idx)
            #     print("the probability after looping through one student: ",y)

            #     opt.zero_grad()
            #     loss = binary_cross_entropy().mean()
            #     loss.backward()
            #     opt.step()

            #     loss_mean.append(loss.detach().cpu().numpy())

            # for t in test: 
            #     q, r = t

            #     self.eval()



