import pickle
import argparse
import numpy as np
import torch
from torch.nn.functional import binary_cross_entropy
from torch.utils.data import DataLoader, random_split 
from torch.optim import SGD, Adam
from sklearn import metrics
from dkt_new import DKT
from dkvmn_new import DKVMN
from sakt_new import SAKT 

import random 

if torch.cuda.is_available():
    from torch.cuda import FloatTensor
    torch.set_default_tensor_type(torch.cuda.FloatTensor)
    
else:
    from torch import FloatTensor

def main():
    num_epochs = 500
    batch_size = 64
    hidden_size = 200

    with open(".datasets/u_list.pkl", "rb") as f:
        u_list = pickle.load(f)
    with open(".datasets/u2idx.pkl","rb") as f:
        u2idx = pickle.load(f)
    with open(".datasets/q_list.pkl", "rb") as f:
        q_list = pickle.load(f)
    with open(".datasets/q2idx.pkl", "rb") as f:
        q2idx = pickle.load(f)

    #important 
    with open(".datasets/q_seq.pkl", "rb") as f:
        q_seq = pickle.load(f)
    with open(".datasets/r_seq.pkl", "rb") as f:
        r_seq = pickle.load(f)
    with open(".datasets/t_seq.pkl", "rb") as f:
        t_seq = pickle.load(f)
    with open(".datasets/d_seq.pkl", "rb") as f:
        d_seq = pickle.load(f)
    with open(".datasets/m_seq.pkl", "rb") as f:
        m_seq = pickle.load(f)
    

    q_seq, r_seq, t_seq, d_seq, m_seq = np.array(q_seq), np.array(r_seq), np.array(t_seq), np.array(d_seq), np.array(m_seq)
    print(np.shape(q_seq))
    print(np.shape(t_seq))
    #split into training and test dataset
    total = np.array(range(0,len(q_seq)))
    random.shuffle(total)
    train_size = int(len(q_seq) * 0.9)
    print(q_seq[total[:train_size]])

    # print("shape of q_seq: ",np.shape(q_seq))
    train = q_seq[total[:train_size]], r_seq[total[:train_size]], t_seq[total[:train_size]], d_seq[total[:train_size]], m_seq[total[:train_size]]
    test = q_seq[total[train_size:]], r_seq[total[train_size:]], t_seq[total[train_size:]], d_seq[total[train_size:]], m_seq[total[train_size:]]

    # train_loader = DataLoader(train, batch_size = 64, shuffle = True)
    # test_loader = DataLoader(test, batch_size = 64, shuffle = True)

    # MODEL DECLARATION 

    model = DKT(hidden_size, len(q_list))
    # model = DKVMN(len(q_list), 50, 50, 20)
    # model = SAKT(len(q_list), 30, 200, 5, 0.2) 
    opt = Adam(model.parameters(), lr = 0.001)

  
    # Call train_model function to run the deep learning model 
    # model.train_model(train, test, q2idx, num_epochs, hidden_size, opt, batch_size)
    model.train_model(train, test, q2idx, num_epochs, hidden_size, opt, batch_size)

main()

    # num_kcs = M_train
    # train_size = r_train.shape[0]
    # model = DKT(hidden_size, num_kcs)
    # opt = ADAM(model.parameters(), lr = 0.001)
    # aucs,loss_means = [], []
    # for i in range(1, num_epochs + 1):
    #     loss_means = []
    #     for _ in range(train_size // batch_size ):
    #         bch_idx = np.random.choice(train_size, batch_size, replace = False)
    #         bch_features = FloatTensor(t)