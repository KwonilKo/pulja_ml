from os import stat_result
import numpy as np
import torch
from torch.nn import Module, Parameter, Embedding, Softmax, Tanh, Linear, ReLU, Sequential, MultiheadAttention, LayerNorm, Dropout  
from torch import nn
from torch.nn.functional import one_hot, binary_cross_entropy
from torch.nn.init import normal_
from sklearn import metrics

if torch.cuda.is_available():
    torch.set_default_tensor_type(torch.cuda.FloatTensor)

class SAKT(Module):
    def __init__(self, num_kcs, d, n, num_heads, dropout):
        super(SAKT, self).__init__()
        print(num_kcs)
        # hyper parameters 
        self.num_kcs = num_kcs 
        self.d = d 
        self.n = n 
        self.num_heads = num_heads 
        self.dropout = dropout 

        # model parameters 
        self.M = Embedding(2 * self.num_kcs, self.d)
        self.E = Embedding(self.num_kcs, self.d)
        self.P = Parameter(torch.Tensor(self.n, self.d))

        normal_(self.P)

        self.attn = MultiheadAttention(
            self.d, self.num_heads, dropout = self.dropout
        )
        self.attn_dropout = Dropout(self.dropout)
        self.attn_layer_norm = LayerNorm([self.d])

        self.feed_forward = Sequential(
            Linear(self.d, self.d),
            ReLU(), 
            Dropout(self.dropout),
            Linear(self.d, self.d),
            Dropout(self.dropout),
        )

        self.feed_forward_layer_norm = LayerNorm([self.d])
        self.prediction = Linear(self.d, 1)


    def forward(self, q_seq, r_seq, d_seq):

        qr = np.array(self.num_kcs * r_seq + q_seq)
        M = self.M(torch.tensor(qr).to(torch.long)).permute(1,0,2)
        # print("Shape of M_s: ", M.size())

        P = self.P.unsqueeze(0).permute(1,0,2)
        # print("Shape of P: ", self.P.unsqueeze(0).permute(1,0,2).size())

        M += P 
        # print("addition ", (M + P).size())

        # make sure you only consider first t interactions when predicting the result of t+1 interactions 
        E = self.E(torch.tensor(d_seq).to(torch.long)).permute(1,0,2)
        # print("E_size: ", E.size()[0])

        temp = torch.triu(torch.ones(E.size()[0], M.size()[0]), diagonal = 1).bool()
        attn_output, attn_weights = self.attn(E, M, M, attn_mask = temp)
        attn_output = self.attn_layer_norm(attn_output) # layer normalization 

        #feed forward 
        F = self.feed_forward(attn_output)
        F = self.feed_forward_layer_norm(F)
        # print("F shape: ", F.size())
        
        #prediction 
        prob = torch.sigmoid(self.prediction(F)).squeeze(-1).permute(1,0)        
        return prob

    

    def train_model(self, train, test, q2idx, num_epochs, hidden_size, opt, batch_size):
        
        aucs, loss_means = [], []
        q, r, t, d, m = train 
        q_test, r_test, t_test, d_test, m_test = test 

        for i in range(1, num_epochs + 1): 
            auc, loss_mean = [], []
            bch_idx = np.random.choice(len(q), batch_size, replace = False)
            q_seq, r_seq, t_seq, d_seq, m_seq = q[bch_idx], r[bch_idx], t[bch_idx], d[bch_idx], m[bch_idx]

            self.train()
            prob = self(q_seq, r_seq, d_seq)
            prob = torch.masked_select(prob, torch.tensor(m_seq))
            true = torch.masked_select(torch.tensor(r_seq), torch.tensor(m_seq))

            opt.zero_grad()
            loss = binary_cross_entropy(prob, true)
            
            loss.backward()
            opt.step()

            loss_mean.append(loss.detach().cpu().numpy())

            with torch.no_grad(): 
                bch_idx = np.random.choice(len(q_test), batch_size, replace = False)
                q_seq, r_seq, t_seq, d_seq, m_seq = q_test[bch_idx], r_test[bch_idx], t_test[bch_idx], d_test[bch_idx], m_test[bch_idx]
                self.eval() 

                test_prob = self(q_seq,r_seq, d_seq)
                test_prob = torch.masked_select(test_prob, torch.tensor(m_seq)).detach().cpu()
                test_true = torch.masked_select(torch.tensor(r_seq), torch.tensor(m_seq)).float().detach().cpu()

                auc = metrics.roc_auc_score(
                    y_true = test_true.numpy(), y_score = test_prob.numpy()
                )

                loss_mean = np.mean(loss_mean)
                
                print(
                    "Epoch: {},   AUC: {},   Loss Mean: {}"
                    .format(i, auc, loss_mean)
                )

                aucs.append(auc)
                loss_means.append(loss_mean)

        return aucs, loss_means



