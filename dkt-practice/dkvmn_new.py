from os import stat_result
import numpy as np
import torch
from torch.nn import Module, Parameter, Embedding, Softmax, Tanh, Linear  
from torch import nn
from torch.nn.functional import one_hot, binary_cross_entropy
from torch.nn.init import normal_
from sklearn import metrics

if torch.cuda.is_available():
    torch.set_default_tensor_type(torch.cuda.FloatTensor)

class DKVMN(Module):
    def __init__(self, num_kcs, d_k, d_v, d_n):
        super(DKVMN, self).__init__()

        # hyper parameters
        self.num_kcs = num_kcs
        self.d_k = d_k
        self.d_v = d_v
        self.d_n = d_n 


        # model parameters 
        self.A = Embedding(self.num_kcs, self.d_k)
        self.B = Embedding(2 * self.num_kcs, self.d_v)
        self.M_k = Parameter(torch.Tensor(self.d_n, self.d_k))
        self.M_v = Parameter(torch.Tensor(self.d_n, self.d_v))
        self.D = nn.Linear(self.d_v, self.d_v)
        self.E = nn.Linear(self.d_v, self.d_v)

        self.fc1 = nn.Linear(2 * self.d_k, self.d_k)
        self.p1 = nn.Linear(self.d_k, 1)

        normal_(self.M_k)
        normal_(self.M_v)

    def forward(self, q_seq, r_seq):
        M_v_temp = self.M_v.unsqueeze(0)
        prob = []
        for q, r in zip(q_seq, r_seq):
            # print(len(q))
            # print(self.M_v.unsqueeze(0).size())
            k_t = self.A(torch.tensor(q).to(torch.long))
            # print("k_t : ",k_t.size())
            new_r =  torch.tensor(np.array(self.num_kcs * r + q)).to(torch.long)
            w_t = torch.softmax(torch.matmul(self.M_k, k_t.T), dim = -1).T
            # print("w_t size: ", torch.matmul(self.M_k, k_t.T).size())
            # read process 
            r_t = (w_t.unsqueeze(-1) * M_v_temp).sum(1)
            # r_t = torch.matmul(w_t, self.M_v)
            # print("w_t size: ", w_t.size())
            # print("r_t size: ", r_t.size())
            # print("fully size: ", ( torch.cat([r_t, k_t], dim = -1) ).size())
            f_t = torch.tanh(self.fc1( torch.cat([r_t, k_t], dim = -1) ))
            # print("f_t size: ", f_t.size())
            p_t = torch.sigmoid(self.p1 ( f_t )).squeeze()
            # print("p_t size: ", p_t.size())

            # write process
            v_t = self.B(new_r)
            e_t = torch.sigmoid(self.E(v_t))
            # e_t = torch.sigmoid(torch.matmul(self.E, v_t.T)).T
            # print("e_t size: ", e_t.size())
            # print("temp size: ", temp.size())
            # print("w_t * e_t size: ", (w_t.unsqueeze(-1) * e_t.unsqueeze(1)).size()  )
            M_v_temp = M_v_temp * (1 - w_t.unsqueeze(-1) * e_t.unsqueeze(1))
            a_t = torch.tanh(self.D(v_t))
            # a_t = torch.tanh(torch.matmul(v_t, self.D))

            M_v_temp = M_v_temp + w_t.unsqueeze(-1) * a_t.unsqueeze(1)
            prob.append(p_t)
            # print(p_t)
            # print("p_t shape: ", np.shape(np.array(prob)))
            # print(prob)
        result = torch.stack(prob, dim = 0)
        # print("shape of result: ", result.size())
            
        return result


        # for idx2, one_student in enumerate(q): 
            
        #     for idx2, one_question in enumerate(one_student):
        #        k_t = self.A(one_question)
        #        w_t = Softmax(torch.matmuul(self.M_k, k_t))     
               
        #        # read process 
        #        r_t = torch.matmul(w_t, self.M_v)
        #        f_t = Tanh()



    def train_model(self, train, test, q2idx, num_epochs, hidden_size, opt, batch_size):
        
        aucs, loss_means = [], []
        q, r, t, d, m = train 
        q_test, r_test, t_test, d_test, m_test = test 

        for i in range(1, num_epochs + 1): 
            auc, loss_mean = [], []
            bch_idx = np.random.choice(len(q), batch_size, replace = False)
            q_seq, r_seq, t_seq, d_seq, m_seq = q[bch_idx], r[bch_idx], t[bch_idx], d[bch_idx], m[bch_idx]

            self.train()
            prob = self(q_seq, r_seq)
            prob = torch.masked_select(prob, torch.tensor(m_seq))
            true = torch.masked_select(torch.tensor(r_seq), torch.tensor(m_seq))

            opt.zero_grad()
            loss = binary_cross_entropy(prob, true)

            loss.backward()
            opt.step()

            loss_mean.append(loss.detach().cpu().numpy())

            with torch.no_grad(): 
                bch_idx = np.random.choice(len(q_test), batch_size, replace = False)
                q_seq, r_seq, t_seq, d_seq, m_seq = q_test[bch_idx], r_test[bch_idx], t_test[bch_idx], d_test[bch_idx], m_test[bch_idx]
                self.eval() 

                test_prob = self(q_seq,r_seq)
                test_prob = torch.masked_select(test_prob, torch.tensor(m_seq)).detach().cpu()
                test_true = torch.masked_select(torch.tensor(r_seq), torch.tensor(m_seq)).float().detach().cpu()

                auc = metrics.roc_auc_score(
                    y_true = test_true.numpy(), y_score = test_prob.numpy()
                )

                loss_mean = np.mean(loss_mean)
                
                print(
                    "Epoch: {},   AUC: {},   Loss Mean: {}"
                    .format(i, auc, loss_mean)
                )

                aucs.append(auc)
                loss_means.append(loss_mean)

        return aucs, loss_means



