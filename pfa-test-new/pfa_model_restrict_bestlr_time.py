import torch

from torch.nn import Module, Parameter
from torch.nn.functional import softplus
from torch.nn.init import normal_
import numpy as np 
if torch.cuda.is_available():
    torch.set_default_tensor_type(torch.cuda.FloatTensor)


class PFARESTRICT(Module):
    def __init__(self, num_user, num_kc):
        super(PFARESTRICT, self).__init__()

        self.num_user = num_user
        self.num_kc = num_kc
        print(num_kc)

        self.w1 = Parameter(torch.Tensor(self.num_kc + 1)) # +1 for adding total failure
        self.w2 = Parameter(torch.Tensor(self.num_kc + 1)) # +1 for adding total success
        self.w3 = Parameter(torch.Tensor(self.num_kc))

        # for PFAE, adding time parameters, torch size = length of time windows  
        self.w4 = Parameter(torch.Tensor(5))

        normal_(self.w1)
        normal_(self.w2)
        normal_(self.w3)
        # for PFAE, adding time parameters, torch size = length of time windows 
        normal_(self.w4)

    def forward(self, indicators, features):
        
        w1 = -softplus(self.w1)
        w2 = softplus(self.w2)
        w3 = self.w3

        # for PFAE, adding time parameters, torch size = length of time windows 
        w4 = self.w4

        w = torch.cat([w1, w2, w3, w4])
        
        fail, succ, ind, time = indicators[:, :self.num_kc], \
            indicators[:, self.num_kc: 2*self.num_kc], \
            indicators[:, 2*self.num_kc: 3*self.num_kc], \
                indicators[:, 3*self.num_kc:]

        features_size = np.shape(features)[0] 
        indices = np.ones((features_size,1))
        fail = np.hstack((fail, indices))
        succ = np.hstack((succ, indices))

        indicators = np.concatenate((fail, succ, ind, time), axis = 1)
        # indicators = torch.from_numpy(indicators).float()
        
        X = features * indicators
        X = X.float()
        y = torch.matmul(X, w)
        p = torch.sigmoid(y)

        return p

    def get_knowledge_state(self, features):
        with torch.no_grad():
            # print("features shape: ", features.shape)
            # print("number of Kcs: ", self.num_kc)
            feature_size = features.shape[0]
            # print(features[:, :self.num_kc].shape)
            # print(torch.eye(self.num_kc).shape)

            failure = features[:, :self.num_kc].unsqueeze(-1)\
                * torch.eye(self.num_kc)
            count_fail = features[:, self.num_kc]
            success = features[:, self.num_kc + 1 : 2 * self.num_kc + 1].unsqueeze(-1)\
                * torch.eye(self.num_kc)
            count_succ = features[:, 2 * self.num_kc + 1]
            X = torch.cat(
                [
                    failure,
                    success,
                    (torch.eye(self.num_kc).unsqueeze(0)
                        .repeat(feature_size, 1, 1))
                ],
                dim=-1
            )
            print(X.shape)
            
            w1 = -softplus(self.w1)
            w2 = softplus(self.w2)
            w3 = self.w3

            w = torch.cat([w1[:-1], w2[:-1], w3])
            
            y = torch.matmul(X, w)
            #for adding features of total succ and total fail 
            # X_ = torch.reshape(torch.cat([count_fail*w1[-1], count_succ*w2[-1]], dim = -1), (feature_size, 2))
            # print(X_.size())
            
            # total_sum = y + torch.sum(X_)
            # p = torch.sigmoid(total_sum)
            p = torch.sigmoid(y)
            return p