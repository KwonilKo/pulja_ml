import torch

from torch.nn import Module, Parameter
from torch.nn.functional import softplus
from torch.nn.init import normal_
import numpy as np 
if torch.cuda.is_available():
    torch.set_default_tensor_type(torch.cuda.FloatTensor)


class PFARESTRICT(Module):
    def __init__(self, num_user, num_kc):
        super(PFARESTRICT, self).__init__()

        self.num_user = num_user
        self.num_kc = num_kc

        self.w1 = Parameter(torch.Tensor(self.num_kc + 1)) # +1 for adding total failure
        self.w2 = Parameter(torch.Tensor(self.num_kc + 1)) # +1 for adding total success
        self.w3 = Parameter(torch.Tensor(self.num_kc))

        # for PFAE, adding time parameters, torch size = length of time windows  
        # self.w4 = Parameter(torch.Tensor(5))

        normal_(self.w1)
        normal_(self.w2)
        normal_(self.w3)
        # for PFAE, adding time parameters, torch size = length of time windows 
        # normal_(self.w4)

    def forward(self, indicators, features):
        
        w1 = -softplus(self.w1)
        w2 = softplus(self.w2)
        w3 = self.w3

        # for PFAE, adding time parameters, torch size = length of time windows 
        # w4 = self.w4

        w = torch.cat([w1, w2, w3])
        
        fail, succ, ind = np.hsplit(indicators, 3)
        features_size = np.shape(features)[0] 
        indices = np.ones((features_size,1))
        fail = np.hstack((fail, indices))
        succ = np.hstack((succ, indices))

        indicators = np.concatenate((fail, succ, ind), axis = 1)
        # indicators = torch.from_numpy(indicators).float()
        
        X = features * indicators
        X = X.float()
        y = torch.matmul(X, w)
        p = torch.sigmoid(y)

        return p

    def get_knowledge_state(self, features):
        with torch.no_grad():
            # print("features shape: ", features.shape)
            # print("number of Kcs: ", self.num_kc)
            feature_size = features.shape[0]
            # print(features[:, :self.num_kc].shape)
            # print(torch.eye(self.num_kc).shape)

            failure = features[:, :self.num_kc + 1].unsqueeze(-1)\
                * torch.eye(self.num_kc)
            success = features[:, self.num_kc + 1 : 2 * self.num_kc + 2].unsqueeze(-1)\
                * torch.eye(self.num_kc)
            # indicator = features[:, self.num_kc * 2 : self.num_kc * 3].unsqueeze(-1)\
            #     * torch.eye(self.num_kc)
            # time = features[:, self.num_kc * 3:].unsqueeze(-1)\
            #     * torch.eye(5)
        
            # print(time.shape)

            # X = torch.cat(
            #     [
            #         failure,
            #         success,
            #         indicator,
            #         time.repeat(1, int(self.num_kc / 5), 1)
            #     ],
            #     dim=-1
            # )
            # X = torch.cat(
            #     [
            #         failure,
            #         success
            #     ],
            #     dim=-1
            # )
            X = torch.cat(
                [
                    failure,
                    success,
                    (torch.eye(self.num_kc).unsqueeze(0)
                        .repeat(feature_size, 1, 1))
                ],
                dim=-1
            )
            print(X.shape)
            
            w1 = -softplus(self.w1)
            w2 = softplus(self.w2)
            w3 = self.w3

            # print("some values of w1")
            # print(w1[:100])
            # print("some values of w2")
            # print(w2[:100])
            # print("some values of w3")
            # print(w3[:100])

            # when time parameter is added 
            # w4 = self.w4

            w = torch.cat([self.w1, self.w2, self.w3])
            # w = torch.cat([self.w1, self.w2])
            # print("first couple lines of failure")
            # print(failure[:50])
            # print("first couple lines of success")
            # print(success[:50])

            y = torch.matmul(X, w)
            p = torch.sigmoid(y)
            # print("knowledge state probability: ", p)
            return p
