import pickle
import datetime
import pandas as pd
import numpy as np
import torch
import matplotlib.pyplot as plt

from torch import FloatTensor
from scipy.special import logit
# from pfa_model import PFA 
# from pfa_model_new import PFANEW
from pfa_model_restrict_bestlr import PFARESTRICT
# from pfa_model_restrict import PFARESTRICT
# from pfa_model_noh import PFANOH
# from pfa_time_model import PFATIME

from sklearn.metrics import roc_curve, roc_auc_score, accuracy_score
from math import pi
from matplotlib.path import Path
from matplotlib.spines import Spine
from matplotlib.transforms import Affine2D
from matplotlib import rc
rc('font', family = 'AppleGothic')
plt.rcParams['axes.unicode_minus'] = False

def map_kc(kc_name, top5kcs):
    with open(".datasets/kc_{}2idx_name_pfa.pkl".format(kc_name), "rb") as f:
        kc_name_hash = pickle.load(f)
    save = []
    for kc in top5kcs: 
        values = [int(v) for v in kc_name_hash.values()]
        temp = list(kc_name_hash.keys())[values.index(kc)]
        save.append(temp)
    return save 
             
def choose_biggest_change(target, num, boolean, kc_name):
    with open(".datasets/{}_knowledge_state_final.pkl".format(target), "rb") as f:
        kc_prob_final = pickle.load(f)
        print("target: ", target)
        # print("Final probability: ", kc_prob_final)
    with open(".datasets/{}_knowledge_state_initial.pkl".format(target), "rb") as f:
        kc_prob_initial = pickle.load(f)
        print("target: ", target)
        # print("Initial probability: ", kc_prob_initial)
    # 개념, 카테고리, 스킬, 조건 probability 
    with open(".datasets/{}_kc_name_info.pkl".format(target), "rb") as f:
        kc_name_info = pickle.load(f)
    
    each_kc_final, each_kc_initial = {}, {} 
    for idx, kc in enumerate(kc_name_info): 
        each_kc_final[kc] = kc_prob_final[idx]
        # print("Before applying SIGMOID FUNCTION  final probability:sdsds", sig)
        # print("final probability:sdsds", each_kc_final[kc])
        each_kc_initial[kc] = kc_prob_initial[idx]
   
    # find the 5 biggest change of KC in each 개념, 카테고리, 스킬, 조건 and return it accordingly
    each_kc_biggest_change_info = {} 
    for idx, kc in enumerate(kc_name_info):
        kc_final, kc_initial = each_kc_final[kc], each_kc_initial[kc]
        diff_hash = {}
        for (k1,fin), (k2,init) in zip(kc_final.items(), kc_initial.items()):
            diff_hash[k1] = (fin - init)
        diff_result = sorted(diff_hash, key = diff_hash.get, reverse = boolean)[: num]
        each_kc_biggest_change_info[kc] = diff_result

    return each_kc_initial, each_kc_final, each_kc_biggest_change_info




def represent_state(concepts, categories, conditions, skills, unit2seqs, unit3seqs):
    total = [concepts, categories, conditions, skills, unit2seqs, unit3seqs]
    total_bool = np.array([ 1 if t is not None else 0 for t in total])
    name = np.array(["concepts", "categories", "conditions", "skills", "unit2seqs", "unit3seqs"])
    temp = [name[i] for i,u in enumerate(total_bool) if u == 1]
    
    with open(".datasets/cbt4_user_list_pfa.pkl", "rb") as f:
        cbt4_user = pickle.load(f)
    print(cbt4_user)
    
    for user in cbt4_user: 
        with open(".datasets/{}_kc_name_info.pkl".format(user), "rb") as f: 
            kc_name = pickle.load(f)
        for kc in kc_name: 
            boolean = [True, False]
            for b in boolean:
                init, final, kc_change = choose_biggest_change(user, 5, b, temp)
                top_5_kcs = kc_change[kc]
                init_kc = [init[kc][i] for i in top_5_kcs]
                final_kc = [final[kc][i] for i in top_5_kcs]
                variant_p = [init_kc, final_kc]
                # print(user, kc," kc ","initial probability: ", init_kc)
                # print(user, kc," kc ","final probability:", final_kc)     

                #draw the plot and save it 
                ## 하나로 합치기 - 폴리곤
                top_5_kcs = map_kc(kc, top_5_kcs)

                angles = [x / 5 * (2 * pi) for x in range(len(top_5_kcs))] ## 각 등분점
                angles += angles[:1] ## 시작점으로 다시 돌아와야하므로 시작점 추가
                my_palette = plt.cm.get_cmap("Set2", len(variant_p))
                fig = plt.figure(figsize=(8,8))
                fig.set_facecolor('white')
                ax = fig.add_subplot(polar=True)
                for i, (curr_p, label) in enumerate(zip(variant_p, ["Before", "After"])):
                    color = my_palette(i)
                    data = curr_p
                    data = np.concatenate([curr_p, [curr_p[0]]], axis=-1)
                    ax.set_theta_offset(pi / 2) ## 시작점
                    ax.set_theta_direction(-1) ## 그려지는 방향 시계방향
                    plt.xticks(angles[:-1], top_5_kcs, fontsize=13) ## x축 눈금 라벨
                    ax.tick_params(axis='x', which='major', pad=15) ## x축과 눈금 사이에 여백을 준다.
                    ax.set_rlabel_position(0) ## y축 각도 설정(degree 단위)
                    plt.yticks([.0, .2, .4, .6, .8, 1.0],['0.0','0.2','0.4','0.6','0.8','1.0'], fontsize=10) ## y축 눈금 설정
                    plt.ylim(0, 1)
                    ax.plot(angles, data, color=color, linewidth=2, linestyle='solid', label=label) ## 레이더 차트 출력
                    ax.fill(angles, data, color=color, alpha=0.4) ## 도형 안쪽에 색을 채워준다.
                for g in ax.yaxis.get_gridlines(): ## grid line
                    g.get_path()._interpolation_steps = len(top_5_kcs)
                spine = Spine(
                    axes=ax,
                    spine_type='circle',
                    path=Path.unit_regular_polygon(len(top_5_kcs))
                )
                ## Axes의 중심과 반지름을 맞춰준다.
                spine.set_transform(Affine2D().scale(.5).translate(.5, .5) + ax.transAxes)
                ax.spines = {'polar':spine} ## frame의 모양을 원에서 폴리곤으로 바꿔줘야한다.
                plt.legend(loc=(0.9,0.9))
                plt.title("Target User: {} , Target KC: {}".format(user, kc), fontsize=10, pad=25)
                time = datetime.datetime.now()
                if b is True: 
                    message = "positive"
                else:
                    message = "negative"
                plt.savefig("{}_representation/pfa_restrict_{}_{}_representation_{}_{}.png".format(kc,user, kc, message, time))

