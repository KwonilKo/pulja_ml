import pandas as pd
import numpy as np
import pickle
import random 

import pandas as pd 
import random 

def preprocess (concepts, categories, conditions, skills, unit2seq, unit3seqs):
    
    dataset_path = ".datasets/cbt123_categories_concept_condition_skill_unit2seq.csv"
    df = pd.read_csv(dataset_path, sep = '\t')
    df = df.rename(columns={'# username': 'username', ' userSeq': 'userSeq', ' question_id': 'question_id', ' isCorrect': 'isCorrect', ' regdate': 'regdate', ' concepts\t\t\t\t\t':'concepts' })
    df = df.rename(columns={' concepts': 'concepts', ' skills': 'skills', ' conditions': 'conditions',' categories': 'categories', 'category2': 'unit2seqs', 'category3': 'unit3seqs'})
    df = df.fillna("0")
    name = [concepts, categories,conditions, skills, unit2seq, unit3seqs]
    name_hash = {0: "concepts", 1: "categories", 2: "conditions", 3: "skills", 4: "unit2seqs", 5: 'unit3seqs'}
    result_idx, result_list = {}, {}

    for i,n in enumerate(name): 
        if n is not None and i < 4:
            kc = name_hash[i]
            df[kc] = df[kc].map(lambda x: x.strip().split("|"))
            df[kc] = df[kc].map(lambda x: np.array(x).astype(int) if (x != ['']) else np.array([0]))
            temp_list = []
            for d in df[kc].values:
                temp_list+= d.tolist()
            kc_list = np.sort(np.unique(temp_list))
            result_list[kc] = kc_list 
            result_idx[kc] = {n:m for m,n in enumerate(kc_list)}
            with open(".datasets/kc_{}2idx_pfa.pkl".format(kc), "wb") as f:
                pickle.dump(result_idx[kc], f)
            
            #preprocessing concept, categories, conditions, skills 
            kcpath =".datasets/tb_{}.csv".format(kc)
            temp = pd.read_csv(kcpath, sep = '\t')
            temp_hash = {} 
            temp_values = temp.values
            for ii,t in temp_values:
                temp_hash[t] = ii
            #default value for zero 
                temp_hash["0"] = 0
            # print(temp_hash)
            with open(".datasets/kc_{}2idx_name_pfa.pkl".format(kc), "wb") as f:
                pickle.dump(temp_hash, f)
        else: 
            kc = name_hash[i]
            temp = np.unique(df[kc].values)
            temp_hash = {}
            for i,u in enumerate(temp):
                temp_hash[u] = i
            with open(".datasets/kc_{}2idx_name_pfa.pkl".format(kc), "wb") as f:
                pickle.dump(temp_hash, f)
            for ii, r in df.iterrows():
                save = r[kc]
                df.at[ii, kc] = np.array([temp_hash[save]])
            temp_list = []
            for d in df[kc].values:
                temp_list += d.tolist()
            kc_list = np.sort(np.unique(temp_list))
            result_list[kc] = kc_list 
            result_idx[kc] = {n:m for m,n in enumerate(kc_list)}
            with open(".datasets/kc_{}2idx_pfa.pkl".format(kc),"wb") as f:
                pickle.dump(result_idx[kc], f)            
    with open(".datasets/preprocessed_data_pfa.pkl", "wb") as f:
        pickle.dump(df, f)
    

    return df, result_list, result_idx

def split_train_test(df): 

    train_size = int(0.8 * len(df))

    total = np.arange(len(df))
    random.shuffle(total)

    train_list = np.sort(total[:train_size])
    test_list = np.sort(total[train_size:])

    df_train = df.loc[train_list, :]
    train_user_list = np.unique(df_train['username'].values)

    df_test = df.loc[test_list, :]
    test_user_list = np.unique(df_test['username'].values)

    with open(".datasets/train_user_list_pfa.pkl", "wb") as f:
        pickle.dump(train_user_list, f)
        
    with open(".datasets/test_user_list_pfa.pkl", "wb") as f:
        pickle.dump(test_user_list, f)

    return df_train, df_test, train_user_list, test_user_list


def train_preprocess(df, train_user_list, kc_concept2idx , kc_categories2idx, kc_condition2idx, kc_skill2idx, kc_unit2seqs2idx, kc_unit3seqs2idx):
    
    train_indicators = []
    train_features = []
    train_labels = []

    name = np.array(["concepts", "categories", "conditions", "skills", "unit2seqs", "unit3seqs"])
    total = np.array([kc_concept2idx, kc_categories2idx, kc_condition2idx, kc_skill2idx, kc_unit2seqs2idx, kc_unit3seqs2idx])
    total_bool = np.array([1 if t is not None else 0 for t in total])    
    
    total_kc = list(filter(lambda x: x is not None, total))
    total_name = np.array([ name[i] for i,t in enumerate(total_bool) if t is not None ])

    for user_name in train_user_list:
        df_for_oneuser = df[df["username"] == user_name]
        features = []

        for i in range(len(df_for_oneuser)):
            kc_save = [df_for_oneuser.iloc[i][total_name[j]] if t == 1  else 0 for j,t in enumerate(total_bool) ]            
            kc_indicator = np.zeros([np.sum(np.array([len(t) for t in total_kc]))])
            
            label = int(df_for_oneuser.iloc[i]["isCorrect"] == "Y")
            track, idx, default_len  = 0, [], np.sum(np.array([len(t) for t in total_kc]))
            feature = np.zeros([2 * default_len])

            for ii,b in enumerate(total_bool): 
                if b == 1: 
                    for k in kc_save[ii]:
                        kc_indicator[total[ii][k] + track] += 1
                        idx += [total[ii][k] + track + (default_len * label)]
                    track += len(total[ii])
           
            for id in idx:
                feature[id] += 1
            features.append(feature)
            train_indicators.append(kc_indicator)
            train_labels.append(label)
        
        features = np.cumsum(features, axis = 0)
        features[1:] = features[:-1]
        features[0] = np.zeros([2 * default_len])
        features = np.log(1 + features)
        train_features.append(features)
        

    train_indicators = np.array(train_indicators)
    train_features = np.hstack([np.vstack(train_features), train_indicators])
    train_indicators = np.hstack([train_indicators] * 3)
    train_labels = np.array(train_labels)
    train_dataset = train_indicators, train_features, train_labels

    with open(".datasets/train_dataset_pfa.pkl", "wb") as f:
        pickle.dump(train_dataset, f)
    
    return train_dataset 

def test_preprocess(df, test_user_list, kc_concept2idx , kc_categories2idx, kc_condition2idx, kc_skill2idx, kc_unit2seqs2idx, kc_unit3seqs2idx):
    test_indicators = []
    test_features = []
    test_labels = []

    total = np.array([kc_concept2idx, kc_categories2idx, kc_condition2idx, kc_skill2idx, kc_unit2seqs2idx, kc_unit3seqs2idx])
    total_bool = np.array([1 if t is not None else 0 for t in total])    
    total_kc = list(filter(lambda x: x is not None, total))
    name = np.array(["concepts", "categories", "conditions", "skills", "unit2seqs", "unit3seqs"])
    total_name = np.array([ name[i] for i,t in enumerate(total_bool) if t is not None ])


    for user_name in test_user_list:
        df_for_oneuser = df[df["username"] == user_name]
        features = []

        for i in range(len(df_for_oneuser)):
            kc_save = [df_for_oneuser.iloc[i][total_name[j]] if t == 1  else 0 for j,t in enumerate(total_bool) ]  
            kc_indicator = np.zeros([np.sum(np.array([len(t) for t in total_kc]))])
            
            label = int(df_for_oneuser.iloc[i]["isCorrect"] == "Y")
            track, idx, default_len  = 0, [], np.sum(np.array([len(t) for t in total_kc]))
            feature = np.zeros([2 * default_len])

            for ii,b in enumerate(total_bool): 
                if b == 1 : 
                    for k in kc_save[ii]:
                        kc_indicator[total[ii][k] + track] += 1
                        idx += [total[ii][k] + track + (default_len * label)]
                    track += len(total[ii])
           
            for id in idx:
                feature[id] += 1
            features.append(feature)
            test_indicators.append(kc_indicator)
            test_labels.append(label)
        
        features = np.cumsum(features, axis = 0)
        features[1:] = features[:-1]
        features[0] = np.zeros([2 * default_len])
        features = np.log(1 + features)
        test_features.append(features)

    test_indicators = np.array(test_indicators)
    test_features = np.hstack([np.vstack(test_features), test_indicators])
    test_indicators = np.hstack([test_indicators] * 3)
    test_labels = np.array(test_labels)

    test_dataset = test_indicators, test_features, test_labels

    with open(".datasets/test_dataset_pfa.pkl", "wb") as f:
        pickle.dump(test_dataset, f)

    return test_dataset 

def kc_list_and_idx(kc_concept2idx, kc_categories2idx, kc_condition2idx, kc_skill2idx, kc_unit2seqs2idx, kc_unit3seqs2idx):
    total = [kc_concept2idx, kc_categories2idx, kc_condition2idx, kc_skill2idx, kc_unit2seqs2idx, kc_unit3seqs2idx]
    total_eff = list(filter(lambda x: x is not None, total))
    
    kc_list = np.concatenate([t for t in total_eff], axis = None)
    kc2idx, temp = {}, 0 
    for kc_specific in total_eff:
        for k in kc_specific: 
            kc2idx[temp + k] = kc_specific[k]
        temp += len(kc_specific)

    with open(".datasets/kc_list_pfa.pkl", "wb") as f:
        pickle.dump(kc_list, f)
   
    with open(".datasets/kc2idx_pfa.pkl", "wb") as f:
        pickle.dump(kc2idx, f)

    return kc_list, kc2idx  

