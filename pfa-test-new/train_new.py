import pickle
import argparse
import numpy as np
import torch

from torch.nn.functional import binary_cross_entropy
from torch.optim import SGD
from torch.optim import Adam


from sklearn import metrics


# from pfa_model_new import PFANEW
# from pfa_model_restrict_bestlr import PFARESTRICT
from pfa_model_restrict_bestlr_time import PFARESTRICT
# from pfa_model_restrict import PFARESTRICT
# from pfa_model_noh import PFANOH


from eachstudent_knowledge import preprocess_eachstudent, each_student_rep

from data_preprocess_organized_bestlr_time import preprocess, split_train_test, train_preprocess, test_preprocess, kc_list_and_idx
# from data_preprocess_organized_bestlr import preprocess, split_train_test, train_preprocess, test_preprocess, kc_list_and_idx
# from data_preprocess_organized import preprocess, split_train_test, train_preprocess, test_preprocess, kc_list_and_idx


from Knowledge_state_depiction_bestlr_time import run_model, prob_kc_target, total_user_knowledge_state
# from Knowledge_state_depiction_organized_bestlr import run_model, prob_kc_target, total_user_knowledge_state
# from Knowledge_state_depiction_organized import run_model, prob_kc_target, total_user_knowledge_state

from Knowledge_state_representation_users import choose_biggest_change, represent_state

if torch.cuda.is_available():
    print("I am using GPU")
    from torch.cuda import FloatTensor
    torch.set_default_tensor_type(torch.cuda.FloatTensor)
else:
    print("I am not using GPU")
    from torch import FloatTensor

# this is the main_helper function that aims to find the best-fitting train/test dataset 
# and will save this train/test dataset and send it to main() 
def main_helper_preprocess (concepts, categories, conditions, skills, unit2seqs, unit3seqs): 


    #preprocess the data according to the parsed arguments
    df, kc_list, kc_idx = preprocess(concepts, categories, conditions, skills, unit2seqs, unit3seqs)
    df_train, df_test, train_user_list, test_user_list = split_train_test(df)

    save_pars_idx = []
    for kc in [concepts, categories, conditions, skills, unit2seqs, unit3seqs]:
        if kc == None: 
            save_pars_idx += [None]
        else:
            save_pars_idx += [kc_idx[kc]]
    train_dataset = train_preprocess(df_train, train_user_list, save_pars_idx[0], save_pars_idx[1], save_pars_idx[2], save_pars_idx[3], save_pars_idx[4], save_pars_idx[5])
    test_dataset = test_preprocess(df_test, test_user_list, save_pars_idx[0], save_pars_idx[1], save_pars_idx[2], save_pars_idx[3], save_pars_idx[4], save_pars_idx[5])
    kc_list, kc2idx = kc_list_and_idx(save_pars_idx[0], save_pars_idx[1], save_pars_idx[2], save_pars_idx[3], save_pars_idx[4], save_pars_idx[5])

    return train_user_list, test_user_list, train_dataset, test_dataset, kc_list, kc2idx, save_pars_idx 

# run the model and returns aucs, losses 
def main_helper_run(model, opt, train_size, batch_size, num_epochs, train_dataset, test_dataset): 
    
    train_indicators, train_features, train_labels = train_dataset
    test_indicators, test_features, test_labels = test_dataset   
    
    aucs = []
    loss_means = []

    for i in range(1, num_epochs + 1):
        loss_mean = []

        for _ in range(train_size // batch_size):
            bch_idx = np.random.choice(train_size, batch_size, replace=False)

            bch_indicators = FloatTensor(train_indicators[bch_idx])
            # print("shape of indicator: ", len(bch_indicators))
            bch_features = FloatTensor(train_features[bch_idx])
            # print("shape of features: ", len(bch_features))
            bch_labels = FloatTensor(train_labels[bch_idx])

            model.train()

            bch_p = model(bch_indicators, bch_features)

            # 형철님이 주신 dataset 이용할때 
            # print("shape of bch_p: ", bch_p.size())
            # bch_labels = bch_labels.squeeze(1)

            opt.zero_grad()
            loss = binary_cross_entropy(bch_p, bch_labels).mean()
            loss.backward()
            opt.step()

            loss_mean.append(loss.detach().cpu().numpy())

        with torch.no_grad():
            model.eval()
            # print("passed here")
            p = model(
                FloatTensor(test_indicators), FloatTensor(test_features)
            ).detach().cpu()
            auc = metrics.roc_auc_score(
                y_true=test_labels, y_score=p.numpy()
            )

            loss_mean = np.mean(loss_mean)

            print(
                "Epoch: {},   AUC: {},   Loss Mean: {}"
                .format(i, auc, loss_mean)
            )

            aucs.append(auc)
            loss_means.append(loss_mean)

    return aucs, loss_means 


def helper(data, concepts, categories, conditions, skills, unit2seqs, unit3seqs):    
    #split the data into concept, categories, conditions, skills 
    with open(".datasets/kc_concepts2idx_pfa.pkl","rb") as f: 
        temp_concepts = pickle.load(f)
        len_concepts = len(temp_concepts)
    with open(".datasets/kc_categories2idx_pfa.pkl","rb") as f: 
        temp_categories = pickle.load(f)
        len_categories =len(temp_categories)
    with open(".datasets/kc_conditions2idx_pfa.pkl","rb") as f: 
        temp_conditions = pickle.load(f)
        len_conditions = len(temp_conditions)
    with open(".datasets/kc_skills2idx_pfa.pkl","rb") as f: 
        temp_skills = pickle.load(f)
        len_skills = len(temp_skills)
    with open(".datasets/kc_unit2seqs2idx_pfa.pkl","rb") as f: 
        temp_unit2seqs = pickle.load(f)
        len_unit2seqs = len(temp_skills)
    with open(".datasets/kc_unit3seqs2idx_pfa.pkl","rb") as f: 
        temp_unit3seqs = pickle.load(f)
        len_unit3seqs = len(temp_unit3seqs)
    
    a_concepts, a_categories, a_conditions, a_skills, a_unit2seqs, a_unit3seqs = \
        data[:,:len_concepts], data[:,len_concepts:len_concepts+len_categories], \
             data[:,len_concepts+len_categories:len_concepts+len_categories+len_conditions], \
                 data[:,len_concepts+len_categories+len_conditions: len_concepts+len_categories+len_conditions+len_skills], \
                     data[:, len_concepts+len_categories+len_conditions+len_skills:len_concepts+len_categories+len_conditions+len_skills+len_unit2seqs], \
                         data[:, len_categories+len_conditions+len_skills+len_unit2seqs:len_categories+len_conditions+len_skills+len_unit2seqs+len_unit3seqs]
    
    temp = [concepts, categories, conditions, skills, unit2seqs, unit3seqs] 
    result = [a_concepts, a_categories, a_conditions, a_skills, a_unit2seqs, a_unit3seqs]
    save = np.concatenate([result[i] for i,r in enumerate(temp) if r !=  None ], axis = 1)
  
    return save 


def helper_preprocess(total_dataset, concepts, categories, conditions, skills, unit2seqs, unit3seqs): 
    total_train_user_list, total_test_user_list, total_train_dataset, total_test_dataset, \
        total_kc_list, total_kc2idx, total_save_pars_idx = total_dataset 
    
    #based on the features we need, split the train and test features and indicators 
    total_train_indicators, total_train_features, total_train_labels = total_train_dataset 
    total_test_indicators, total_test_features, total_test_labels = total_test_dataset 

    #--------------train----------------#
    #필요한 train features 들만 추출
    train_fail, train_succ, train_ind = np.array_split(total_train_features, 3, axis = 1)
    train_fail = helper(train_fail, concepts, categories, conditions, skills, unit2seqs, unit3seqs)
    train_succ = helper(train_succ, concepts, categories, conditions, skills, unit2seqs, unit3seqs) 
    train_ind = helper(train_ind, concepts, categories, conditions, skills, unit2seqs, unit3seqs)
    #update the train_features 
    total_train_features = np.hstack([train_fail, train_succ, train_ind])

    #필요한 train indicators 들만 추출 
    train_i, _, _ = np.array_split(total_train_indicators, 3, axis = 1)
    train_i = helper(train_i, concepts, categories, conditions, skills, unit2seqs, unit3seqs)
    #update the train_indicators
    total_train_indicators = np.hstack([train_i] * 3) 

    #--------------test----------------#
    #필요한 test features 들만 추출
    test_fail, test_succ, test_ind = np.array_split(total_test_features, 3, axis = 1)
    test_fail = helper(test_fail, concepts, categories, conditions, skills, unit2seqs, unit3seqs)
    test_succ = helper(test_succ, concepts, categories, conditions, skills, unit2seqs, unit3seqs) 
    test_ind = helper(test_ind, concepts, categories, conditions, skills, unit2seqs, unit3seqs)
    #update the test_features 
    total_test_features = np.hstack([test_fail, test_succ, test_ind])

    #필요한 test indicators 들만 추출 
    test_i, _, _ = np.array_split(total_test_indicators, 3, axis = 1)
    test_i = helper(train_i, concepts, categories, conditions, skills, unit2seqs, unit3seqs)
    #update the test_indicators
    total_test_indicators = np.hstack([test_i] * 3) 

    #update the train and test dataset 
    total_train_dataset = total_train_indicators, total_train_features, total_train_labels 
    total_test_dataset = total_test_indicators, total_test_features, total_test_labels 

    return total_train_dataset, total_test_dataset 



def main(concepts, categories, conditions, skills, unit2seqs, unit3seqs):
    default_opt = [SGD, Adam]
    default_opt_name = ["SGD", "ADAM"]
    default_lr, track_lr_len = [0.1, 0.03, 0.009], 0
    default_auc, track_auc = 0.685, 0
    num_epochs = 500
    batch_size = 128
    track_iterations = 0

    while (track_auc < default_auc):
        print("Here is the number of attempted iterations to find the dataset: ", track_iterations)
        
        # first, process concepts, categories, conditions, skills 한꺼번에 
        total_dataset = main_helper_preprocess("concepts", "categories", "conditions", "skills", "unit2seqs", "unit3seqs")
        # total_dataset = main_helper_preprocess(concepts, categories, conditions, skills, unit2seqs, unit3seqs)

        # and then based on user's requests, 필요한 features들만 추출 
        train_dataset, test_dataset = helper_preprocess(total_dataset, concepts, categories, conditions, skills, unit2seqs, unit3seqs)

        train_user_list, test_user_list, train_dataset, test_dataset, kc_list, kc2idx, save_pars_idx = main_helper_preprocess(concepts, categories, conditions, skills, unit2seqs, unit3seqs)
        num_user = len(train_user_list) + len(test_user_list)
        num_kc = 0
        for i,k in enumerate(kc_list):
            num_kc += len(kc_list[i])
        train_size = train_dataset[2].shape[0]
        model = PFARESTRICT(num_user, num_kc)
        # model = PFANEW(num_user, num_kc)
        # model = PFANOH(num_user, num_kc)

        for learn in default_lr: 
            for i, optimizer in enumerate(default_opt): 
                opt = optimizer(model.parameters(), lr = learn)
                aucs, loss_means = main_helper_run(model, opt, train_size, batch_size, num_epochs, train_dataset, test_dataset)
                track_auc = aucs[-1]
                # checking if aucs of last epoch is bigger than 0.66  
                if track_auc >= default_auc: 
                    print("Escaping optimizer loop..")
                    opt_name_save = default_opt_name[i]
                    break
                else: 
                    print("Shit, wrong optimizer..")
                    print("WRONG optimizer name: ", default_opt_name[i], "WRONG learning rate: ", learn, ", auc: ", aucs[-1])
            #checking if aucs is bigger than 0.66 
            if track_auc >= default_auc: 
                print("Model found the appropriate dataset. CORRECT optimizer: ",opt_name_save," and CORRECT learning rate: ", learn)
                break 
            else: 
                print("Shit, learning rate", learn, " didn't work let me try again with different learning rate")
                track_lr_len += 1
        if track_auc >= default_auc: 
            print("Model found the appropriate dataset, optimizer, and learning rate!!, which has auc of ", track_auc, "optimizer of ", opt_name_save,
            " and learning rate of ", learn)
        else: 
            print("Unfortunately, model cound't find appropriate learning rate and optimizer for this training and test dataset")
            track_iterations += 1 

    # once you found the appropriate train/test dataset that returns the auc of bigger than 0.66, you run the whole process
    print("Finally, we run the whole process")
    


    # Also, make sure to download the founded training and test dataset 
    with open(".datasets/train_dataset_pfa_auc={}_learning_rate={}_optimizer={}.pkl".format(track_auc, learn, opt_name_save), "wb") as f: 
        pickle.dump(train_dataset, f)
    with open(".datasets/test_dataset_pfa_auc={}_learning_rate={}_optimizer={}.pkl".format(track_auc, learn, opt_name_save), "wb") as f: 
        pickle.dump(test_dataset, f)
    with open(".datasets/train_user_list_pfa_auc={}_learning_rate={}_optimizer={}.pkl".format(track_auc, learn, opt_name_save), "wb") as f: 
        pickle.dump(train_user_list, f)
    with open(".datasets/test_user_list_pfa_auc={}_learning_rate={}_optimizer={}.pkl".format(track_auc, learn, opt_name_save), "wb") as f: 
        pickle.dump(test_user_list, f)
    print("Succesfully dumped train and test dataset with high auc, learning rate, and optimizer information")

    #--------------RUN THE WHOLE PROCESS HERE----------------# 
    if (concepts != None or categories != None):
        if (concepts != None):
            kc = concepts 
        else:
            kc = categories 
        print("processing....")
        df, _, _ = preprocess_eachstudent(concepts, categories, None, None, None, None)
        each_student_rep(df, kc)
    _, state = run_model(concepts, categories, conditions, skills, unit2seqs, unit3seqs)
    print("train_new state: ", state)
    total_user_knowledge_state(state, concepts, categories, conditions, skills, unit2seqs, unit3seqs)
    represent_state(concepts, categories, conditions, skills, unit2seqs, unit3seqs)
    



def str_bool(v):
    if v.lower() == "none": 
        return None 
    else: 
        return v.lower() 

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    
    parser.add_argument(
        "concepts",
        type = str_bool, default = True,
    )
    parser.add_argument(
        "categories",
        type = str_bool, default = False,
    )
    parser.add_argument(
        "conditions",
        type = str_bool, default = False,
    )
    parser.add_argument(
        "skills",
        type = str_bool, default = False,
    )
    parser.add_argument(
        "unit2seqs",
        type = str_bool, default = False,
    )
    parser.add_argument(
        "unit3seqs",
        type = str_bool, default = False,
    )

    args = parser.parse_args()
    print(args.concepts)
    print(args.categories)
    print(args.conditions)
    print(args.skills)
    print(args.unit2seqs)
    print(args.unit3seqs)


    main(args.concepts, args.categories, args.conditions, args.skills, args.unit2seqs, args.unit3seqs)
