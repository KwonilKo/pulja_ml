import pickle
import argparse
import numpy as np
import torch

from torch.nn.functional import binary_cross_entropy
from torch.optim import SGD
from torch.optim import Adam


from sklearn import metrics

# from pfa_model_new import PFANEW
# from pfa_model_restrict import PFARESTRICT
# from pfa_model_noh import PFANOH
from pfa_model_restrict_bestlr import PFARESTRICT

if torch.cuda.is_available():
    from torch.cuda import FloatTensor
    torch.set_default_tensor_type(torch.cuda.FloatTensor)
else:
    from torch import FloatTensor


def main(learning_rate, o):
    num_epochs = 500
    batch_size = 128

    print("Here is the learning rate: ", learning_rate, "and optimizer: ", o)
    with open(".datasets/train_user_list_pfa.pkl", "rb") as f:
        train_user_list = pickle.load(f)
        # print(train_user_list)
    with open(".datasets/test_user_list_pfa.pkl", "rb") as f:
        test_user_list = pickle.load(f)
        # print(test_user_list)
    with open(".datasets/kc_list_pfa.pkl", "rb") as f:
        kc_list = pickle.load(f)
    # with open(".datasets/kc2idx.pkl", "rb") as f:
    #     kc2idx = pickle.load(f)

    with open(".datasets/train_dataset_pfa.pkl", "rb") as f:
        train_dataset = pickle.load(f)
    print("Here is the shape of train dataset: ", np.shape(train_dataset[0]))
    # print("here is the problem train_dataset size: ", np.shape(train_dataset[0]))
    # print("here is the problem train_dataset size: ", np.shape(train_dataset[1]))
    # print("here is the problem train_dataset size: ", np.shape(train_dataset[2]))

    with open(".datasets/test_dataset_pfa.pkl", "rb") as f:
        test_dataset = pickle.load(f)

    # num_user = train_user_list.shape[0] + test_user_list.shape[0]
    num_user = len(train_user_list) + len(test_user_list)

    num_kc = 0
    for i,k in enumerate(kc_list):
        num_kc += len(kc_list[i])
    # num_kc = len(kc_list)
    # num_kc = len(kc_list["concepts"])

    # num_kc = kc_list.shape[0]/3 + (kc_list.shape[0]/3) * 2 * 5 #window size = 5

    train_indicators, train_features, train_labels = train_dataset
    test_indicators, test_features, test_labels = test_dataset

    train_size = train_labels.shape[0]
    # test_size = test_labels.shape[0]

    # model = PFANEW(num_user, num_kc) 
    model = PFARESTRICT(num_user, num_kc)
    # model = PFANOH(num_user,num_kc)
    # model = PFARESTRICT(num_user, num_kc)

    if o == "SGD": 
        opt = SGD(model.parameters(), lr=learning_rate)
    else:
        opt = Adam(model.parameters(), lr=learning_rate)

    aucs = []
    loss_means = []

    for i in range(1, num_epochs + 1):
        loss_mean = []

        for _ in range(train_size // batch_size):
            bch_idx = np.random.choice(train_size, batch_size, replace=False)

            bch_indicators = FloatTensor(train_indicators[bch_idx])
            # print("shape of indicator: ", len(bch_indicators))
            bch_features = FloatTensor(train_features[bch_idx])
            # print("shape of features: ", len(bch_features))
            bch_labels = FloatTensor(train_labels[bch_idx])

            model.train()

            bch_p = model(bch_indicators, bch_features)

            # 형철님이 주신 dataset 이용할때 
            # print("shape of bch_p: ", bch_p.size())
            # bch_labels = bch_labels.squeeze(1)

            opt.zero_grad()
            loss = binary_cross_entropy(bch_p, bch_labels).mean()
            loss.backward()
            opt.step()

            loss_mean.append(loss.detach().cpu().numpy())

        with torch.no_grad():
            model.eval()
            # print("passed here")
            p = model(
                FloatTensor(test_indicators), FloatTensor(test_features)
            ).detach().cpu()
            auc = metrics.roc_auc_score(
                y_true=test_labels, y_score=p.numpy()
            )

            loss_mean = np.mean(loss_mean)

            print(
                "Epoch: {},   AUC: {},   Loss Mean: {}"
                .format(i, auc, loss_mean)
            )

            aucs.append(auc)
            loss_means.append(loss_mean)

    # print(model.w[:num_kc])
    # print(model.w[num_kc:2 * num_kc])
    # print(model.w[2 * num_kc:])

    with open(".ckpts/loss_means_new_lr={}_opt={}.pkl".format(learning_rate, o), "wb") as f:
        pickle.dump(loss_means, f)
    with open(".ckpts/aucs_new_lr={}_opt={}.pkl".format(learning_rate, o), "wb") as f:
        pickle.dump(aucs, f)

    torch.save(model.state_dict(), ".ckpts/model_new_aucs={}_lr={}_opt={}.ckpt".format(aucs[-1], learning_rate, o))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--learning_rate",
        type = float
    )
    args = parser.parse_args()

    main(args.learning_rate)
