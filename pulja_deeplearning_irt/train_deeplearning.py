import pickle
import argparse
import numpy as np
import torch
from torch.nn.functional import binary_cross_entropy
from torch.utils.data import DataLoader, random_split 
from torch.optim import SGD, Adam
from sklearn import metrics

from model.dkt import DKT
from model.sakt import SAKT
from model.ssakt import SSAKT
from model.dkvmn import DKVMN


import random 

if torch.cuda.is_available():
    from torch.cuda import FloatTensor
    torch.set_default_tensor_type(torch.cuda.FloatTensor)
    
else:
    from torch import FloatTensor

def main(model_name):
    num_epochs = 200
    batch_size = 32
    hidden_size = 200

    with open(".datasets/u_list_{}.pkl".format(model_name), "rb") as f:
        u_list = pickle.load(f)
    with open(".datasets/u2idx_{}.pkl".format(model_name),"rb") as f:
        u2idx = pickle.load(f)
    with open(".datasets/q_list_{}.pkl".format(model_name), "rb") as f:
        q_list = pickle.load(f)
    with open(".datasets/q2idx_{}.pkl".format(model_name), "rb") as f:
        q2idx = pickle.load(f)

    #important 
    with open(".datasets/q_seq_{}.pkl".format(model_name), "rb") as f:
        q_seq = pickle.load(f)
    with open(".datasets/r_seq_{}.pkl".format(model_name), "rb") as f:
        r_seq = pickle.load(f)
    with open(".datasets/t_seq_{}.pkl".format(model_name), "rb") as f:
        t_seq = pickle.load(f)
    with open(".datasets/d_seq_{}.pkl".format(model_name), "rb") as f:
        d_seq = pickle.load(f)
    with open(".datasets/m_seq_{}.pkl".format(model_name), "rb") as f:
        m_seq = pickle.load(f)
    

    q_seq, r_seq, t_seq, d_seq, m_seq = np.array(q_seq), np.array(r_seq), np.array(t_seq), np.array(d_seq), np.array(m_seq)

    #split into training and test dataset
    total = np.array(range(len(q_seq)))
    random.shuffle(total)
    print("here is the size of q_seq: ", np.shape(q_seq))
    print("here is the size of r_seq: ", np.shape(r_seq))

    train_size = int(len(q_seq) * 0.8)

    # print("shape of q_seq: ",np.shape(q_seq))
    train = q_seq[total[:train_size]], r_seq[total[:train_size]], t_seq[total[:train_size]], d_seq[total[:train_size]], m_seq[total[:train_size]]
    test = q_seq[total[train_size:]], r_seq[total[train_size:]], t_seq[total[train_size:]], d_seq[total[train_size:]], m_seq[total[train_size:]]

    # MODEL DECLARATION 
    if model_name == "SAKT":
        model = SAKT(len(q_list), 30, 100, 5, 0.2)
    elif model_name == "DKT":
        model = DKT(hidden_size, len(q_list))
    elif model_name == "DKVMN":
        model = DKVMN(len(q_list), 50, 50, 20)
    elif model_name == "SSAKT":
        model = SSAKT(len(q_list), 100, 30, 5, 3, 0.2)
    
    # optimizer declaration
    opt = Adam(model.parameters(), lr = 0.001)

  
    # Call train_model function to run the deep learning model 
    # model.train_model(train, test, q2idx, num_epochs, hidden_size, opt, batch_size)
    aucs, loss_means, state = model.train_model(train, test, q2idx, num_epochs, hidden_size, opt, batch_size)
    return aucs, loss_means, state
