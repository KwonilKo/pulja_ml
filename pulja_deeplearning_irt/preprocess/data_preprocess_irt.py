import pandas as pd
import numpy as np
import pickle
import random 

def split_train_test(df): 

    train_size = int(0.8 * len(df))

    total = np.arange(len(df))
    random.shuffle(total)

    train_list = np.sort(total[:train_size])
    test_list = np.sort(total[train_size:])

    df_train = df.loc[train_list, :]
    train_user_list = np.unique(df_train['userSeq'].values)

    df_test = df.loc[test_list, :]
    test_user_list = np.unique(df_test['userSeq'].values)

    with open(".datasets/train_user_list_pfa.pkl", "wb") as f:
        pickle.dump(train_user_list, f)
        
    with open(".datasets/test_user_list_pfa.pkl", "wb") as f:
        pickle.dump(test_user_list, f)

    return df_train, df_test, train_user_list, test_user_list

def question_train_test_preprocess():
    dataset_path = ".datasets/total_log.csv" 
    df = pd.read_csv(dataset_path, sep = '\t')
    df = df.rename(columns={'# username': 'username', ' userSeq': 'userSeq', ' question_id': 'question_id', ' isCorrect': 'isCorrect', 'regdate': 'start', ' concepts\t\t\t\t\t':'concepts' })
    df = df.rename(columns={' concepts': 'concepts', ' skills': 'skills', ' conditions': 'conditions',' categories': 'categories', 'category2': 'unit2seqs', 'category3': 'unit3seqs', 
    'category4': 'unit4seqs', 'category5': 'unit5seqs'})    
    df = df.rename(columns = {'unit1seq': 'unit0seqs', 'unit2seq': 'unit1seqs'}) 
    df = df.fillna("0")
    
    #write total problems and useres in pickle
    total_users = np.unique(df["userSeq"].values)
    total_problems = np.unique(df["question_id"].values)

    with open(".datasets/total_users.pkl", "wb") as f:
        pickle.dump(total_users, f)
    with open(".datasets/total_problems.pkl", "wb") as f:
        pickle.dump(total_problems, f)
    
    problems_solved = {}
    for i,r in df.iterrows():
        if r["question_id"] not in problems_solved.keys():
            problems_solved[r["question_id"]] = 1
        else:
            problems_solved[r["question_id"]] += 1
    with open(".datasets/problem_solved.pkl", "wb") as f:
        pickle.dump(problems_solved, f)

    user_solved_problem = {}
    for i, r in df.iterrows():
        if r["userSeq"] not in user_solved_problem.keys():
            user_solved_problem[r["userSeq"]] = [r["question_id"]]
        else:
            user_solved_problem[r["userSeq"]] += [r["question_id"]]
    with open(".datasets/user_solved_problem.pkl","wb") as f:
        pickle.dump(user_solved_problem, f)

    #split data into train/test
    df_train, df_test, train_user_list, test_user_list = split_train_test(df)
    return df_train, df_test, train_user_list, test_user_list
    
    
def question_train_preprocess(df, train_user_list):
    student_indicators = []    
    train_indicators = []
    train_labels = []
    with open(".datasets/total_users.pkl", "rb") as f:
        total_users = pickle.load(f)
    with open(".datasets/total_problems.pkl", "rb") as f:
        total_problems = pickle.load(f)

    for user_seq in train_user_list:
        df_for_oneuser = df[df["userSeq"] == user_seq]
        features = []

        for i in range(len(df_for_oneuser)):
            #problem indicator
            problem_id = df_for_oneuser.iloc[i]["question_id"]
            kc_indicator = np.zeros([len(total_problems)])
            kc_idx = np.where(total_problems == problem_id)
            kc_indicator[kc_idx] = 1
            
            #student indicator
            student_indicator = np.zeros([len(total_users)])
            std_idx = np.where(total_users == user_seq)
            student_indicator[std_idx] = 1

            #label 
            label = int(df_for_oneuser.iloc[i]["isCorrect"] == "Y")
            
            train_indicators.append(kc_indicator)
            student_indicators.append(student_indicator)
            train_labels.append(label)

    train_indicators = np.hstack((student_indicators, train_indicators))
    train_labels = np.array(train_labels)
    print("size of train_indicators is :", np.shape(np.array(train_indicators)))
    train_dataset = train_indicators, train_labels 
    with open(".datasets/train_dataset.pkl", "wb") as f:
        pickle.dump(train_dataset, f)
    return train_indicators, train_labels    

def question_test_preprocess(df, test_user_list):
    student_indicators = []    
    test_indicators = []
    test_labels = []
    with open(".datasets/total_users.pkl", "rb") as f:
        total_users = pickle.load(f)
    with open(".datasets/total_problems.pkl", "rb") as f:
        total_problems = pickle.load(f)

    for user_seq in test_user_list:
        df_for_oneuser = df[df["userSeq"] == user_seq]
        features = []

        for i in range(len(df_for_oneuser)):
            #problem indicator
            problem_id = df_for_oneuser.iloc[i]["question_id"]
            kc_indicator = np.zeros([len(total_problems)])
            kc_idx = np.where(total_problems == problem_id)
            kc_indicator[kc_idx] = 1
            
            #student indicator
            student_indicator = np.zeros([len(total_users)])
            std_idx = np.where(total_users == user_seq)
            student_indicator[std_idx] = 1

            #label 
            label = int(df_for_oneuser.iloc[i]["isCorrect"] == "Y")
            
            test_indicators.append(kc_indicator)
            student_indicators.append(student_indicator)
            test_labels.append(label)

    test_indicators = np.hstack((student_indicators, test_indicators))
    test_labels = np.array(test_labels)
    print("size of test_indicators is :", np.shape(np.array(test_indicators)))
    test_dataset = test_indicators, test_labels 
    with open(".datasets/test_dataset.pkl", "wb") as f:
        pickle.dump(test_dataset, f)
    return test_indicators, test_labels    