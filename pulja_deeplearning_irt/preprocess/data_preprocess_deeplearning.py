import pandas as pd
import numpy as np
import pickle
import random 
import torch


def preprocess_difficulty(num_intervals):
    dataset_path = ".datasets/total_log.csv"
    df = pd.read_csv(dataset_path, sep = '\t')
    df = df.rename(columns={'# username': 'username', ' userSeq': 'userSeq', ' question_id': 'question_id', ' isCorrect': 'isCorrect', 'regdate': 'start', ' concepts\t\t\t\t\t':'concepts' })
    df = df.rename(columns={' concepts': 'concepts', ' skills': 'skills', ' conditions': 'conditions',' categories': 'categories', 'category2': 'unit2seqs', 'category3': 'unit3seqs', 
    'category4': 'unit4seqs', 'category5': 'unit5seqs'})    
    df = df.rename(columns = {'unit1seq': 'unit0seqs', 'unit2seq': 'unit1seqs'})
    print(df.columns)
    df = df.fillna("0")
    
    #default value for difficulty 
    df['difficulty'] = -1

    #load trained difficulty for problems 
    with open(".datasets/question_difficulty.pkl", "rb") as f:
        q_d = pickle.load(f)
    for i in range(0,len(df)):
        df.iloc[i]["difficulty"] = -1

    #prepare for labeling difficulty feature
    min_dif = np.min(np.array(list(q_d.values())))
    q_d = {k: v for k, v in sorted(q_d.items(), key = lambda item: item[1], reverse = False)}   #sort q_d by increasing weight(difficulty)
    size = len(q_d) // num_intervals
    sorted_q_d_idx = {key: int(idx/size) for idx, key in enumerate(q_d.keys())}
    df["difficulty"] = df['question_id'].map(lambda x : sorted_q_d_idx[x])
    df["isCorrect"] = df["isCorrect"].map(lambda x: int(x == "Y"))


    # #prepare for labeling difficulty feature
    # min_dif = np.min(np.array(list(q_d.values())))
    # max_dif = np.max(np.array(list(q_d.values())))
    # interval= []
    # for i in range(1,num_intervals-1):
    #     interval += [min_dif + i * (max_dif-min_dif)/num_intervals]
    # interval = np.array(interval)
    # # print(interval)
    # # print(len(interval))
    
    # #iterate through the df and label the difficulty
    # # df['difficulty'] = df['skill_name'].map(lambda x: int((q_d[x]-min_dif)/size)) 
    # for i in range(0,len(df)):
    #     qid = df.iloc[i]["question_id"]
    #     for ii, value in enumerate(interval):
    #         if q_d[qid] < value:
    #             df.at[i, "difficulty"] = ii
    #             break 
    #         elif ii == len(interval)-1:
    #             df.at[i, "difficulty"] = len(interval)
    #     df.at[i, "isCorrect"] = int(df.at[i, "isCorrect"] == "Y")
    # # print(df)
  
    with open(".datasets/df_difficulty_tag.pkl","wb") as f:
        pickle.dump(df,f)
    return df
    
def q_seq_r_seq(df, model_name):
    u_list = np.unique(df["userSeq"].values)
    with open(".datasets/u_list_{}.pkl".format(model_name), "wb") as f:
        pickle.dump(u_list, f)
    num_user = u_list.shape[0]
    # print("The number of total users is:", num_user)
    u2idx = { user: idx for idx, user in enumerate(u_list) }
    with open(".datasets/u2idx_{}.pkl".format(model_name), "wb") as f:
        pickle.dump(u2idx, f)

    q_list = np.unique(df["difficulty"].values)
    num_question = q_list.shape[0]
    with open(".datasets/q_list_{}.pkl".format(model_name), "wb") as f:
        pickle.dump(q_list, f)
    # print("Total number of unique skill is:", num_question)
    q2idx = { question: idx for idx, question in enumerate(q_list) }
    with open(".datasets/q2idx_{}.pkl".format(model_name), "wb") as f:
        pickle.dump(q2idx, f)

    # make q_seq, r_seq, d_seq, t_seq, masks which store the infomration of each user's log 
    q_seq, r_seq,  = [], []

    for user in u_list: 
        df_for_oneuser = df[df["userSeq"] == user]
        q_temp, r_temp = [], []
        
        for i, row in df_for_oneuser.iterrows():
            q_temp.append(q2idx[row["difficulty"]])
            r_temp.append(row["isCorrect"])
        q_seq.append(q_temp)
        r_seq.append(r_temp)    
        
    question = df["difficulty"].values
    num_question = question.shape[0]
    # print("Total number of problems is:",num_question )
    response= df["isCorrect"].values
    num_response = response.shape[0]
    # print("Total number of repsonses is:",num_response )
    return q_seq, r_seq

def match_seq_len(model_name, q_seq, r_seq, seq_len=100, pad_val=-1):
    '''
    I thought ~~~.
    '''
    preprocessed_q_seq = []
    preprocessed_r_seq = []
    
    for q, r in zip(q_seq, r_seq):
        i = 0
        while i + seq_len + 1 < len(q):
            preprocessed_q_seq.append(q[i:i + seq_len + 1])
            preprocessed_r_seq.append(r[i:i + seq_len + 1])

            i += seq_len + 1

        preprocessed_q_seq.append(
            np.concatenate(
                [
                    q[i:],
                    np.array([pad_val] * (i + seq_len + 1 - len(q)))
                ]
            )
        )
        preprocessed_r_seq.append(
            np.concatenate(
                [
                    r[i:],
                    np.array([pad_val] * (i + seq_len + 1 - len(q)))
                ]
            )
        )

    preprocessed_q_seq = np.array(preprocessed_q_seq)
    print(len(preprocessed_q_seq))
    preprocessed_r_seq = np.array(preprocessed_r_seq)

    print(np.shape(preprocessed_q_seq))

    pre_q_seq = torch.Tensor([ p[:-1] for p in preprocessed_q_seq])   
    pre_d_seq = torch.Tensor([ p[1:] for p in preprocessed_q_seq])    
 
    pre_r_seq = torch.Tensor([ r[:-1] for r in preprocessed_r_seq])
    pre_t_seq = torch.Tensor([ r[1:] for r in preprocessed_r_seq])

    masks = (pre_q_seq != pad_val) * (pre_d_seq != pad_val)
    pre_q_seq, pre_d_seq, pre_r_seq, pre_t_seq = \
        pre_q_seq * masks, pre_d_seq * masks, pre_r_seq * masks, pre_t_seq * masks 

    with open(".datasets/q_seq_{}.pkl".format(model_name), "wb") as f:
        pickle.dump(pre_q_seq, f)

    with open(".datasets/r_seq_{}.pkl".format(model_name), "wb") as f:
        pickle.dump(pre_r_seq, f)

    with open(".datasets/d_seq_{}.pkl".format(model_name), "wb") as f:
        pickle.dump(pre_d_seq, f)

    with open(".datasets/t_seq_{}.pkl".format(model_name), "wb") as f:
        pickle.dump(pre_t_seq, f)

    with open(".datasets/m_seq_{}.pkl".format(model_name), "wb") as f:
        pickle.dump(masks, f)

    return pre_q_seq, pre_r_seq, pre_t_seq, pre_d_seq, masks

