import numpy as np
import torch

from torch.nn import Module, Parameter, Embedding, Sequential, Linear, ReLU, \
    MultiheadAttention, LayerNorm, Dropout, ModuleList
from torch.nn.init import normal_
from torch.nn.functional import binary_cross_entropy
from sklearn import metrics


class SSAKT(Module):
    '''
        I implemented this class with reference to: \
        https://pytorch.org/docs/stable/_modules/torch/nn/modules/transformer.html#TransformerEncoderLayer
    '''
    def __init__(self, num_q, n, d, num_attn_heads, num_attn_layers, dropout):
        super(SSAKT, self).__init__()
        self.num_q = num_q
        self.n = n
        self.d = d
        self.num_attn_heads = num_attn_heads
        self.num_attn_layers = num_attn_layers
        self.dropout = dropout

        self.M = Embedding(self.num_q * 2, self.d)
        self.E = Embedding(self.num_q, d)
        self.P = Parameter(torch.Tensor(self.n, self.d))

        normal_(self.P)

        self.attns = ModuleList([
            MultiheadAttention(
                self.d, self.num_attn_heads, dropout=self.dropout
            )
            for _ in range(self.num_attn_layers)
        ])
        self.attn_dropouts = ModuleList([
            Dropout(self.dropout)
            for _ in range(self.num_attn_layers)
        ])
        self.attn_layer_norms = ModuleList([
            LayerNorm([self.d])
            for _ in range(self.num_attn_layers)
        ])

        self.FFNs = ModuleList([
            Sequential(
                Linear(self.d, self.d),
                ReLU(),
                Dropout(self.dropout),
                Linear(self.d, self.d),
                Dropout(self.dropout),
            )
            for _ in range(self.num_attn_layers)
        ])
        self.FFN_layer_norms = ModuleList([
            LayerNorm([self.d])
            for _ in range(self.num_attn_layers)
        ])

        self.pred = Linear(self.d, 1)

    def forward(self, q_seq, r_seq, d_seq):
        qr = np.array(self.num_q * r_seq + q_seq)


        M = self.M(torch.tensor(qr).to(torch.long)).permute(1, 0, 2)
        E = self.E(torch.tensor(d_seq).to(torch.long)).permute(1, 0, 2)
        P = self.P.unsqueeze(0).permute(1, 0, 2)
        

        temp = torch.triu(torch.ones(E.size()[0], M.size()[0]), diagonal = 1).bool()

        M += P

        for attn, attn_dropout, attn_layer_norm, FFN, FFN_layer_norm in zip(
            self.attns, self.attn_dropouts, self.attn_layer_norms, self.FFNs,
            self.FFN_layer_norms
        ):
            S, attn_weights = attn(E, M, M, attn_mask=temp)
            S = attn_dropout(S)

            S = attn_layer_norm(S + M + E)

            F = FFN(S)
            F = FFN_layer_norm(F + S)

            E = F

        F = F.permute(1, 0, 2)

        p = torch.sigmoid(self.pred(F)).squeeze()

        return p, attn_weights

    def train_model(self, train, test, q2idx, num_epochs, hidden_size, opt, batch_size):
        
        aucs, loss_means = [], []
        q, r, t, d, m = train 
        q_test, r_test, t_test, d_test, m_test = test 

        for i in range(1, num_epochs + 1): 
            auc, loss_mean = [], []

            for _ in range(q.shape[0] // batch_size):

                bch_idx = np.random.choice(len(q), batch_size, replace = False)
                q_seq, r_seq, t_seq, d_seq, m_seq = q[bch_idx], r[bch_idx], t[bch_idx], d[bch_idx], m[bch_idx]

                self.train()
                prob, _ = self(q_seq, r_seq, d_seq)
                prob = torch.masked_select(prob, torch.tensor(m_seq))
                true = torch.masked_select(torch.tensor(t_seq), torch.tensor(m_seq))

                opt.zero_grad()
                loss = binary_cross_entropy(prob, true)

                loss.backward()
                opt.step()

                loss_mean.append(loss.detach().cpu().numpy())

            with torch.no_grad(): 
                bch_idx = np.random.choice(len(q_test), batch_size, replace = False)
                q_seq, r_seq, t_seq, d_seq, m_seq = q_test[bch_idx], r_test[bch_idx], t_test[bch_idx], d_test[bch_idx], m_test[bch_idx]
                self.eval() 

                test_prob, _ = self(q_seq,r_seq, d_seq)
                test_prob = torch.masked_select(test_prob, torch.tensor(m_seq)).detach().cpu()
                test_true = torch.masked_select(torch.tensor(t_seq), torch.tensor(m_seq)).float().detach().cpu()

                auc = metrics.roc_auc_score(
                    y_true = test_true.numpy(), y_score = test_prob.numpy()
                )

                loss_mean = np.mean(loss_mean)
                
                print(
                    "Epoch: {},   AUC: {},   Loss Mean: {}"
                    .format(i, auc, loss_mean)
                )

                aucs.append(auc)
                loss_means.append(loss_mean)

        torch.save(self.state_dict(),".ckpts/ssakt_model_new_aucs={}.ckpt".format(aucs[-1]))
        
        default_state = ".ckpts/ssakt_model_new_aucs={}.ckpt".format(aucs[-1])
        return aucs, loss_means, default_state
