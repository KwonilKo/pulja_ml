from os import stat_result
import numpy as np
import torch
from torch import FloatTensor, LongTensor
from torch.nn import Module, Parameter, Embedding, RNN, Linear, LSTM, Dropout, GRU, ReLU
from torch.nn.functional import one_hot, binary_cross_entropy
from torch.nn.init import normal_
from sklearn import metrics

if torch.cuda.is_available():
    torch.set_default_tensor_type(torch.cuda.FloatTensor)

class DKT(Module):
    def __init__(self, hidden_size, num_kcs):
        super(DKT, self).__init__()
        
        
        # hyper parameters
        self.hidden_size = hidden_size
        self.num_kcs = num_kcs


        # rnn, lstm, gru 
        self.rnn = RNN(input_size = self.hidden_size, hidden_size = self.hidden_size, \
            num_layers = 1, )
        self.lstm = LSTM(self.hidden_size, self.hidden_size, batch_first = False)
        self.gru = GRU(self.hidden_size, self.hidden_size, num_layers = 1)

        self.dropout = Dropout()
        self.fc = Linear(self.hidden_size, self.num_kcs) 
        self.relu = ReLU()
            
        # # model parameters
        # self.W_hx = Parameter(torch.Tensor(self.hidden_size, self.hidden_size))
        # self.W_hh = Parameter(torch.Tensor(self.hidden_size, self.hidden_size))
        # self.b_h = Parameter(torch.Tensor(self.hidden_size))
        self.interaction_emb = Embedding(self.num_kcs * 2, self.hidden_size)
        # self.fc = Linear(self.hidden_size, self.num_kcs)
        # # self.W_yh = Parameter(torch.Tensor(self.num_kcs, self.hidden_size))
        # # self.b_y = Parameter(torch.Tensor(self.num_kcs))
        # # self.h_t = nn.init.zeros_(torch.Tensor(self.hidden_size))

        # normal_(self.W_hx)
        # normal_(self.W_hh)
        # normal_(self.b_h)
        # # normal_(self.W_yh)
        # # normal_(self.b_y)

    def forward(self, q, r): 
        # # print("num_kcs: ", self.num_kcs)
        # # LSTM
        # qr = torch.Tensor(self.num_kcs * r + q).to(torch.long)
        # x_t = self.interaction_emb(qr)
        # output, h = self.lstm(x_t)
        # output = self.fc(output)
        # y = self.dropout(output)
        # prob = torch.sigmoid(y)

        # RNN
        qr = torch.Tensor(self.num_kcs * r + q).to(torch.long)
        x_t = self.interaction_emb(qr)
        output, _ = self.rnn(x_t)
        output = self.fc(output)
        # y = self.dropout(output)
        prob = torch.sigmoid(output)

        # GRU 
        # qr = torch.Tensor(self.   num_kcs * r + q).to(torch.long)
        # x_t = self.interaction_emb(qr)
        # output, _ = self.gru(x_t)
        # output = self.fc(self.relu(output))
        # output = self.dropout(output)
        # prob = torch.sigmoid(output)

        return prob


    def get_knowledge_state(self, q, r):
        print("size of q: ", np.shape(np.array(q)))
        x = torch.Tensor(self.num_kcs * r + q).to(torch.long)
        # x = torch.Tensor([[self.num_kcs * rr + qq for qq, rr in zip(q, r)]]).to(torch.long)
        # x = torch.Tensor(self.num_kcs * r + q).to(torch.long)
        # x = torch.LongTensor([self.num_kcs * r + q])
        print("size of x: ", x.size())
        x_t = self.interaction_emb(x)
        print("size of x_t: ", x_t.size())

        output, hidden_state = self.rnn(x_t)
        output = self.fc(output)
        # y = self.dropout(output)
        print("size of y: ")
        print("here is the hidden_state:")
        print(hidden_state)
        y = output.squeeze(0)      #before passing sigmoid function
        # y = torch.sigmoid(y).squeeze()   #after passing sigmoid function
        return y


    
    def train_model(self, train, test, q2idx, num_epochs, hidden_size, opt, batch_size):
        h_t = None
        aucs, loss_means = [], []
        
        q, r, t, d, m = train
        q_test, r_test, t_test, d_test, m_test = test

        for i in range(1, num_epochs + 1):
            auc, loss_mean = [], []
            
            for _ in range(q.shape[0] // batch_size):
                bch_idx = np.random.choice(len(q), batch_size, replace = False)
                q_seq, r_seq, t_seq, d_seq, m_seq = q[bch_idx], r[bch_idx], t[bch_idx], d[bch_idx], m[bch_idx]

                self.train()
                # print("shape of q_seq: ", np.shape(np.array(q_seq)))
                y= self(q_seq, r_seq)
                # print("here is y")
                # print(y)
                # h_t = state.detach()

                temp = one_hot(torch.tensor(d_seq).to(torch.long), self.num_kcs)
                y = (y * temp).sum(-1)
                # print("size of y: ", y.size())
                y_result = torch.masked_select(y, torch.tensor(m_seq))
                t_result = torch.masked_select(torch.tensor(t_seq), torch.tensor(m_seq))
                # print("here is y_result")
                # print(y_result)
                # print("here is t_result")
                
                opt.zero_grad()
                loss = binary_cross_entropy(y_result, t_result)
                # print("loss")
                # print(loss)
                loss.backward(retain_graph=True)
                opt.step() 

                loss_mean.append(loss.detach().cpu().numpy())
            #test
            with torch.no_grad():
                bch_idx = np.random.choice(len(q_test), batch_size, replace = False)
                q_seq, r_seq, t_seq, d_seq, m_seq = q_test[bch_idx], r_test[bch_idx], t_test[bch_idx], d_test[bch_idx], m_test[bch_idx]    
                self.eval() 

                y = self(q_seq, r_seq)
                temp = one_hot(torch.tensor(d_seq).to(torch.long), self.num_kcs)
                y = (y * temp).sum(-1)

                y_result = torch.masked_select(y, torch.tensor(m_seq))
                t_result = torch.masked_select(torch.tensor(t_seq), torch.tensor(m_seq))

                auc = metrics.roc_auc_score(
                    y_true = t_result.numpy(), y_score = y_result.numpy()
                )

                loss_mean = np.mean(loss_mean)
                print(
                    "Epoch: {},   AUC: {},   Loss Mean: {}"
                    .format(i, auc, loss_mean)
                )

                aucs.append(auc)
                loss_means.append(loss_mean)
            
        torch.save(self.state_dict(),".ckpts/dkt_model_new_aucs={}.ckpt".format(aucs[-1]))
        default_state = ".ckpts/dkt_model_new_aucs={}.ckpt".format(aucs[-1])

        return aucs, loss_means, default_state

