import torch

from torch.nn import Module, Parameter
from torch.nn.functional import softplus
from torch.nn.init import normal_
import numpy as np 
if torch.cuda.is_available():
    torch.set_default_tensor_type(torch.cuda.FloatTensor)

class IRT(Module):
    def __init__(self, num_user, num_kc):
        super(IRT, self).__init__()

        self.num_user = num_user
        self.num_kc = num_kc

        self.w1 = Parameter(torch.Tensor(self.num_user)) 
        self.w2 = Parameter(torch.Tensor(self.num_kc)) 

        normal_(self.w1)
        normal_(self.w2)
       

    def forward(self, indicators):

        # w1 = softplus(self.w1)
        # w2 = self.w2 - softplus(self.w2)   #soft minus function 
        # w = torch.cat([w1, w2])

        w = torch.cat([self.w1, self.w2])
        student, kc = indicators[:, :self.num_user], indicators[:, self.num_user: ]
        features = np.concatenate((student, kc), axis = 1)


        y = torch.matmul(torch.Tensor(features), w)
        p = torch.sigmoid(y)
        return p

    