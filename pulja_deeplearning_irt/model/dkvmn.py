from os import stat_result
import numpy as np
import torch
from torch.nn import Module, Parameter, Embedding, Softmax, Tanh, Linear  
from torch import nn
from torch.nn.functional import one_hot, binary_cross_entropy
from torch.nn.init import normal_
from sklearn import metrics
import pickle

if torch.cuda.is_available():
    torch.set_default_tensor_type(torch.cuda.FloatTensor)

class DKVMN(Module):
    def __init__(self, num_kcs, d_k, d_v, d_n):
        super(DKVMN, self).__init__()

        # hyper parameters
        self.num_kcs = num_kcs
        self.d_k = d_k
        self.d_v = d_v
        self.d_n = d_n 


        # model parameters 
        self.A = Embedding(self.num_kcs, self.d_k)
        self.B = Embedding(2 * self.num_kcs, self.d_v)
        self.M_k = Parameter(torch.Tensor(self.d_n, self.d_k))
        self.M_v = Parameter(torch.Tensor(self.d_n, self.d_v))
        self.D = nn.Linear(self.d_v, self.d_v)
        self.E = nn.Linear(self.d_v, self.d_v)

        self.fc1 = nn.Linear(2 * self.d_k, self.d_k)
        self.p1 = nn.Linear(self.d_k, 1)

        normal_(self.M_k)
        normal_(self.M_v)

    def forward(self, q_seq, r_seq):
        M_v = [self.M_v.unsqueeze(0)]
        prob = []
        for q, r in zip(q_seq.T, r_seq.T):
            M_v_temp = M_v[-1]

            # print(len(q))
            # print(self.M_v.unsqueeze(0).size())
            k_t = self.A(torch.tensor(q).to(torch.long))
            # print("k_t : ",k_t.size())
            # w_t = torch.softmax(torch.matmul(k_t, self.M_k.T), dim=-1)
            w_t = torch.softmax(torch.matmul(k_t, self.M_k.T), dim=1)
            # print("w_t size: ", w_t.size())

            # read process 
            r_t = (w_t.unsqueeze(-1) * M_v_temp).sum(1)
            # r_t = torch.matmul(w_t, self.M_v)
            # print("w_t size: ", w_t.size())
            # print("r_t size: ", r_t.size())
            # print("fully size: ", ( torch.cat([r_t, k_t], dim = -1) ).size())
            f_t = torch.tanh(self.fc1(torch.cat([r_t, k_t], dim=-1)))
            # print("f_t size: ", f_t.size())
            p_t = torch.sigmoid(self.p1(f_t)).squeeze()
            # print("p_t size: ", p_t.size())

            # write process
            v_t = self.B(torch.tensor(self.num_kcs * r + q).to(torch.long))
            e_t = torch.sigmoid(self.E(v_t))
            # e_t = torch.sigmoid(torch.matmul(self.E, v_t.T)).T
            # print("e_t size: ", e_t.size())
            # print("w_t size: ", w_t.size())
            # print("w_t * e_t size: ", (w_t.unsqueeze(-1) * e_t.unsqueeze(1)).size()  )
            M_v_temp = M_v_temp * (1 - w_t.unsqueeze(-1) * e_t.unsqueeze(1))
            a_t = torch.tanh(self.D(v_t))
            # print("size of a_t: ", a_t.size())
            # a_t = torch.tanh(torch.matmul(v_t, self.D))

            M_v_temp = M_v_temp + w_t.unsqueeze(-1) * a_t.unsqueeze(1)
            M_v.append(M_v_temp)
            prob.append(p_t)
            # print(p_t)
            # print("p_t shape: ", np.shape(np.array(prob)))
            # print(prob)
        # print(np.shape(np.array(prob)))
        temp1 = torch.stack(prob, dim = 0)
        temp2 = torch.stack(prob, dim = 1)
        # print("size of temp1: ", temp1.size(), " size of temp2: ", temp2.size())
        result = torch.stack(prob, dim = 1)
        # print(result.size())
        # print("shape of result: ", result.size())
            
        return result, M_v


    def get_knowledge_state(self, q, r, M_v):
        result_p = []
        x = torch.LongTensor([self.num_kcs * r + q])
        # M_v_temp = M_v[idx]
        
        k_t = self.A(torch.tensor(x).to(torch.long))
        print(torch.matmul(k_t, self.M_k.T).size())
        print(torch.matmul(k_t, self.M_k.T))
        w_t = torch.softmax(torch.matmul(k_t, self.M_k.T), dim=1)
        print("size of w_t:", w_t.size())
        r_t = (w_t.unsqueeze(-1) * M_v).sum(1)
        print("size of M_v:", M_v.size())
        print("size of r_t: ", r_t.size())
        print("size of k_t:", k_t.size())
        f_t = torch.tanh(self.fc1(torch.cat([r_t, k_t], dim=-1)))
        p_t = self.p1(f_t).squeeze()
        print("shape of p_t: ", p_t)
        result_p.append(p_t)
        
        return result_p

    def train_model(self, train, test, q2idx, num_epochs, hidden_size, opt, batch_size):
        
        aucs, loss_means, M_vs = [], [], []
        q, r, t, d, m = train 
        q_test, r_test, t_test, d_test, m_test = test 

        for i in range(1, num_epochs + 1): 
            auc, loss_mean = [], []

            for _ in range(q.shape[0] // batch_size):
                bch_idx = np.random.choice(len(q), batch_size, replace = False)
                q_seq, r_seq, t_seq, d_seq, m_seq = q[bch_idx], r[bch_idx], t[bch_idx], d[bch_idx], m[bch_idx]

                self.train()
                prob, M_v = self(q_seq, r_seq)
                prob = torch.masked_select(prob, torch.tensor(m_seq))
                true = torch.masked_select(torch.tensor(r_seq), torch.tensor(m_seq))

                opt.zero_grad()
                loss = binary_cross_entropy(prob, true)

                loss.backward()
                opt.step()

                loss_mean.append(loss.detach().cpu().numpy())

            with torch.no_grad(): 
                bch_idx = np.random.choice(len(q_test), batch_size, replace = False)
                q_seq, r_seq, t_seq, d_seq, m_seq = q_test[bch_idx], r_test[bch_idx], t_test[bch_idx], d_test[bch_idx], m_test[bch_idx]
                self.eval() 

                test_prob, test_M_v = self(q_seq,r_seq)
                test_prob = torch.masked_select(test_prob, torch.tensor(m_seq)).detach().cpu()
                test_true = torch.masked_select(torch.tensor(r_seq), torch.tensor(m_seq)).float().detach().cpu()

                auc = metrics.roc_auc_score(
                    y_true = test_true.numpy(), y_score = test_prob.numpy()
                )

                loss_mean = np.mean(loss_mean)
                
                print(
                    "Epoch: {},   AUC: {},   Loss Mean: {}"
                    .format(i, auc, loss_mean)
                )

                aucs.append(auc)
                loss_means.append(loss_mean)
                M_vs.append(test_M_v)

        torch.save(self.state_dict(),".ckpts/dkvmn_model_new_aucs={}.ckpt".format(aucs[-1]))
        default_state = ".ckpts/dkvmn_model_new_aucs={}.ckpt".format(aucs[-1])

        with open(".datasets/dkvmn_mvs.pkl", "wb") as f:
            pickle.dump(M_vs, f)
 
        return aucs, loss_means, default_state




