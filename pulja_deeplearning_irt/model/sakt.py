from os import stat_result
import numpy as np
import torch
from torch.nn import Module, Parameter, Embedding, Softmax, Tanh, Linear, ReLU, Sequential, MultiheadAttention, LayerNorm, Dropout  
from torch import nn
from torch.nn.functional import one_hot, binary_cross_entropy
from torch.nn.init import normal_
from sklearn import metrics

if torch.cuda.is_available():
    torch.set_default_tensor_type(torch.cuda.FloatTensor)

class SAKT(Module):
    def __init__(self, num_kcs, d, n, num_heads, dropout):
        super(SAKT, self).__init__()
        print(num_kcs)
        # hyper parameters 
        self.num_kcs = num_kcs 
        self.d = d 
        self.n = n 
        self.num_heads = num_heads 
        self.dropout = dropout 

        # model parameters 
        self.M = Embedding(2 * self.num_kcs, self.d)
        self.E = Embedding(self.num_kcs, self.d)
        self.P = Parameter(torch.Tensor(self.n, self.d))

        normal_(self.P)

        self.attn = MultiheadAttention(
            self.d, self.num_heads, dropout = self.dropout
        )
        self.attn_dropout = Dropout(self.dropout)
        self.attn_layer_norm = LayerNorm([self.d])

        self.feed_forward = Sequential(
            Linear(self.d, self.d),
            ReLU(), 
            Dropout(self.dropout),
            Linear(self.d, self.d),
            Dropout(self.dropout),
        )

        self.feed_forward_layer_norm = LayerNorm([self.d])
        self.prediction = Linear(self.d, 1)


    def forward(self, q_seq, r_seq, d_seq):

        qr = np.array(self.num_kcs * r_seq + q_seq)
        M = self.M(torch.tensor(qr).to(torch.long)).permute(1,0,2)
        # print("size of M: ", M.size())
        P = self.P.unsqueeze(0).permute(1,0,2)
        # print("size of P: ", P.size())
        # print("Shape of P: ", self.P.unsqueeze(0).permute(1,0,2).size())

        M += P 
        # print("addition ", (M + P).size())

        # make sure you only consider first t interactions when predicting the result of t+1 interactions 
        E = self.E(torch.tensor(d_seq).to(torch.long)).permute(1,0,2)
        # print("E_size: ", E.size()[0])

        temp = torch.triu(torch.ones(E.size()[0], M.size()[0]), diagonal = 1).bool()
        attn_output, attn_weights = self.attn(E, M, M, attn_mask = temp)  #self attention
        attn_output = self.attn_dropout(attn_output)
        attn_output = self.attn_layer_norm( attn_output + M + E) # layer normalization 

        attn_output, M, E = attn_output.permute(1,0,2), M.permute(1,0,2), E.permute(1,0,2)
        # print("attention size: ", attn_output.size(), "M size: ", M.size())
        #feed forward 
        F = self.feed_forward(attn_output)
        # print("F size: ", F.size())
        F = self.feed_forward_layer_norm(F + attn_output)
        # print("F shape: ", F.size())
        # print(self.prediction(F).size())
        #prediction 
        prob = torch.sigmoid(self.prediction(F).squeeze(-1))      
        return prob

    

    def train_model(self, train, test, q2idx, num_epochs, hidden_size, opt, batch_size):
        
        aucs, loss_means = [], []
        q, r, t, d, m = train 
        q_test, r_test, t_test, d_test, m_test = test 

        for i in range(1, num_epochs + 1): 
            auc, loss_mean = [], []

            for _ in range(q.shape[0] // batch_size):

                bch_idx = np.random.choice(len(q), batch_size, replace = False)
                q_seq, r_seq, t_seq, d_seq, m_seq = q[bch_idx], r[bch_idx], t[bch_idx], d[bch_idx], m[bch_idx]

                self.train()
                prob = self(q_seq, r_seq, d_seq)
                prob = torch.masked_select(prob, torch.tensor(m_seq))
                true = torch.masked_select(torch.tensor(t_seq), torch.tensor(m_seq))

                opt.zero_grad()
                loss = binary_cross_entropy(prob, true)

                loss.backward()
                opt.step()

                loss_mean.append(loss.detach().cpu().numpy())

            with torch.no_grad(): 
                bch_idx = np.random.choice(len(q_test), batch_size, replace = False)
                q_seq, r_seq, t_seq, d_seq, m_seq = q_test[bch_idx], r_test[bch_idx], t_test[bch_idx], d_test[bch_idx], m_test[bch_idx]
                self.eval() 

                test_prob = self(q_seq,r_seq, d_seq)
                test_prob = torch.masked_select(test_prob, torch.tensor(m_seq)).detach().cpu()
                test_true = torch.masked_select(torch.tensor(t_seq), torch.tensor(m_seq)).float().detach().cpu()

                auc = metrics.roc_auc_score(
                    y_true = test_true.numpy(), y_score = test_prob.numpy()
                )

                loss_mean = np.mean(loss_mean)
                
                print(
                    "Epoch: {},   AUC: {},   Loss Mean: {}"
                    .format(i, auc, loss_mean)
                )

                aucs.append(auc)
                loss_means.append(loss_mean)

        torch.save(self.state_dict(),".ckpts/sakt_model_new_aucs={}.ckpt".format(aucs[-1]))
        
        default_state = ".ckpts/sakt_model_new_aucs={}.ckpt".format(aucs[-1])
        return aucs, loss_means, default_state



    def get_knowledge_state(self, q, r):
        x = torch.LongTensor([self.num_kcs * r + q])
        kc_list = list(range(self.num_kcs))
        # kc_list = torch.LongTensor([list(range(self.num_kcs))])
        # print("kc_list: ", kc_list)
        result_p = []
        for i,kc in enumerate(kc_list):
            kc = torch.LongTensor([kc])
            M = self.M(x).unsqueeze(0).permute(1,0,2)
            E = self.E(kc).unsqueeze(0).permute(1,0,2)
            P = self.P.unsqueeze(1)

            temp = torch.triu(torch.ones(E.size()[0], M.size()[0]), diagonal = 1).bool()
            M += P[0] 
            S, attn_weights = self.attn(E, M, M, attn_mask = temp)
            S = self.attn_dropout(S)
            S = S.permute(1,0,2)
            M = M.permute(1,0,2)
            E = E.permute(1,0,2)
            S = self.attn_layer_norm(S + M + E)

            F = self.feed_forward(S)
            F = self.feed_forward_layer_norm(F + S)

            # p = self.prediction(F).squeeze()  #before sending sigmoid
            p = torch.sigmoid(self.prediction(F)).squeeze()   #sending sigmoid
            result_p.append(p)
        # print("final size of p: ",np.shape(np.array(result_p)))
        return result_p


