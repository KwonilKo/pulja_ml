import pickle
import pandas as pd
import numpy as np
import torch
import matplotlib.pyplot as plt

from torch import FloatTensor 
from model.dkt import DKT
from model.sakt import SAKT
from model.dkvmn import DKVMN



from sklearn.metrics import roc_curve, roc_auc_score, accuracy_score
from preprocess.data_preprocess_deeplearning import preprocess_difficulty, q_seq_r_seq, match_seq_len

import sys 
from train_deeplearning import main 
import matplotlib.cm as cm
colors = ["r","g","b","c","m","y"]

def run_model(model_name):
    auc_array, loss_array, state = main(model_name)
    #plot auc 
    fig = plt.figure(facecolor=(1,1,1))    
    plt.plot(np.arange(1,100+1), auc_array)
    plt.title("{}_IRT".format(model_name))
    plt.xlabel("Epoch")
    plt.ylabel("Test-AUC")
    fig.savefig("model_result/{}_IRT/AUC_{}.png".format(model_name, auc_array[-1]), bbox_inches='tight')
    plt.show()

    #plot loss 
    fig = plt.figure(facecolor=(1,1,1))    
    plt.plot(np.arange(1,100+1), loss_array)
    plt.title("{}_IRT".format(model_name))
    plt.xlabel("Epoch")
    plt.ylabel("Test-LOSS")
    fig.savefig("model_result/{}_IRT/LOSS_{}.png".format(model_name, loss_array[-1]), bbox_inches='tight')
    plt.show()
    print(state)
    return state

def q_seq_r_seq_oneuser(df, model_name):
    with open(".datasets/q2idx_{}.pkl".format(model_name), "rb") as f:
        q2idx = pickle.load(f)
    q_seq, r_seq,  = [], []
    q_temp, r_temp = [], []
    for i, row in df.iterrows():
        q_temp.append(q2idx[row["difficulty"]])
        r_temp.append(row["isCorrect"])
    q_seq.append(q_temp)
    r_seq.append(r_temp)   
    return q_seq, r_seq

def prob_kc_target(model_name, target, state):
    with open(".datasets/df_difficulty_tag.pkl", "rb") as f:
        df = pickle.load(f)

    df_for_oneuser = df[df["userSeq"] == target]
    q_oneuser, r_oneuser = q_seq_r_seq_oneuser(df_for_oneuser, model_name)
    q_seq, r_seq, t_seq, d_seq, m_seq = match_seq_len(model_name, q_oneuser,r_oneuser)
    q_seq, r_seq, t_seq, d_seq, m_seq = np.array(q_seq), np.array(r_seq), np.array(t_seq), np.array(d_seq), np.array(m_seq)
    
    with open(".datasets/q_list_{}.pkl".format(model_name), "rb") as f:
        q_list = pickle.load(f)

    # load the model 
    device = torch.device("cpu")

    if model_name == "SAKT":
        model = SAKT(len(q_list), 30, 100, 5, 0.2) 
    elif model_name == "DKVMN":
        model = DKVMN(len(q_list), 50, 50, 20)
    elif model_name == "DKT":
        model = DKT(200, len(q_list)) 
        
    model.load_state_dict(torch.load(state, map_location = device))
    model.eval()

    # iterate through user's q_oneuser and r_oneuser and get the probability 
    user_state = []
    for q, r in zip(q_oneuser[0], r_oneuser[0]):
        p = model.get_knowledge_state(q, r)
        p = [pp.detach().numpy() for pp in p]
        user_state.append(p)
    user_state = np.array(user_state)
    # print("shape of user_state is ", np.shape(np.array(user_state))) 
    return user_state

def draw_user_state(model_name, userseq, user_state):
    fig = plt.figure(figsize=[30, 15], facecolor="white")
    user_state = user_state.T 
    print("shape of user_state: ", np.shape(user_state))
    user_state = user_state.mean(axis=0)    #when using mean
    with open(".datasets/user_solved_problem.pkl", "rb") as f:
        user_solved_problem = pickle.load(f)
    target_problems = user_solved_problem[userseq]
    with open(".datasets/df_difficulty_tag.pkl", "rb") as f:
        df = pickle.load(f)

    target_p, target_r = [], []
    for t in target_problems: 
        df_temp = df.loc[df['question_id'] == t, 'difficulty']
        df_temp2 = df[df["userSeq"] == userseq]
        df_temp2 = df_temp2[df_temp2['question_id'] == t]
        response = df_temp2["isCorrect"].values[0]
        difficulty = df_temp.values[0]
        target_p.append(difficulty)
        target_r.append(response)

    show_labels = []
    for p,r in zip(target_p, target_r):
        # print(r)
        if r == 1:
            res = 'o'
        else:
            res = 'x'
        label = str(p)+"("+res+')'
        show_labels.append(label)    
    plt.xticks(np.arange(1,len(target_p)+1, step = 1), show_labels, fontsize = 10, rotation = 20 )
    # plt.pcolormesh(user_state[:,:50], cmap="Reds", vmin = 0, vmax = 1)    #when not using mean
    plt.pcolormesh([user_state[:50]], cmap="Reds", vmin = 0, vmax = 1)      #when using mean
    plt.colorbar()
    plt.show()
    fig.savefig("model_result/{}_student_state_{}mean.png".format(userseq, model_name))