import pickle
import argparse
import numpy as np
import torch

from torch.nn.functional import binary_cross_entropy
from torch.optim import SGD
from torch.optim import Adam


from sklearn import metrics


from model.irt import IRT

if torch.cuda.is_available():
    from torch.cuda import FloatTensor
    torch.set_default_tensor_type(torch.cuda.FloatTensor)
else:
    from torch import FloatTensor


def main(learning_rate, o):
    num_epochs = 50
    batch_size = 128
    
    with open(".datasets/total_users.pkl", "rb") as f:
        total_users = len(pickle.load(f))
    with open(".datasets/total_problems.pkl", "rb") as f:
        total_problems = len(pickle.load(f))

    #load train dataset
    with open(".datasets/train_dataset.pkl", "rb") as f:
        train_dataset = pickle.load(f)

    #load test dataset
    with open(".datasets/test_dataset.pkl", "rb") as f:
        test_dataset = pickle.load(f)

    train_indicators, train_labels = train_dataset
    test_indicators, test_labels = test_dataset
    train_size = train_labels.shape[0]

    #load the model
    model = IRT(total_users, total_problems)
    
    if o == "SGD": 
        opt = SGD(model.parameters(), lr=learning_rate)
    else:
        opt = Adam(model.parameters(), lr=learning_rate)

    aucs = []
    loss_means = []

    for i in range(1, num_epochs + 1):
        loss_mean = []

        for _ in range(train_size // batch_size):
            bch_idx = np.random.choice(train_size, batch_size, replace=False)

            bch_indicators = FloatTensor(train_indicators[bch_idx])
            # print("shape of indicator: ", len(bch_indicators))
            bch_features = FloatTensor(train_indicators[bch_idx])
            # print("shape of features: ", len(bch_features))
            bch_labels = FloatTensor(train_labels[bch_idx])

            model.train()

            bch_p = model(bch_indicators)

            opt.zero_grad()
            loss = binary_cross_entropy(bch_p, bch_labels).mean()
            loss.backward()
            opt.step()

            loss_mean.append(loss.detach().cpu().numpy())

        with torch.no_grad():
            model.eval()
            # print("passed here")
            p = model(
                FloatTensor(test_indicators)
            ).detach().cpu()
            auc = metrics.roc_auc_score(
                y_true=test_labels, y_score=p.numpy()
            )

            loss_mean = np.mean(loss_mean)

            print(
                "Epoch: {},   AUC: {},   Loss Mean: {}"
                .format(i, auc, loss_mean)
            )

            aucs.append(auc)
            loss_means.append(loss_mean)

    # print(model.w[:num_kc])
    # print(model.w[num_kc:2 * num_kc])
    # print(model.w[2 * num_kc:])

    with open(".ckpts/irt_loss_means_new_lr={}_opt={}.pkl".format(learning_rate, o), "wb") as f:
        pickle.dump(loss_means, f)
    with open(".ckpts/irt_aucs_new_lr={}_opt={}.pkl".format(learning_rate, o), "wb") as f:
        pickle.dump(aucs, f)

    torch.save(model.state_dict(), ".ckpts/irt_model_new_aucs={}_lr={}_opt={}.ckpt".format(aucs[-1], learning_rate, o))

